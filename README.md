**MasterMind**

MasterMind is a microservices orchestration platform. The workflow is defined as a JSON and jobs can be submitted to execute the workflow. Core functionality supports just the basic orchestration. Everything else is built as an extension.

There are 2 main services:
1. Executor service - Responsible for executing a workflow. The workflow is provided as a JSON definition.
2. Admin service - Responsible for admin activities like acting as the default end point that the executor publishes the status. reporting service, etc. Typically also hosts the template definitions.

The JSON defintion includes:
1. flowId - uniquely identifies the workflow
2. threadCount - number of parallel threads to creare; default = 1
3. executionStatusUrl - the url to which the status/results are to be published.
2. scenario - the flow definition as a list of tasks.


---

## Parameters

Tasks as well as the whole scenario can have input and output parameters. Each parameter is of the format ${<scope>.<type>.<name>}. Scenario level parameters are global parameters and the scope is always mastermind. Task level parameters are task parameters. the name of the parameter is a JSON path.

For example:
${task1.output.capacityCount}
${task2.input.maxCapacity}
${mastermind.input.totalCapacity}


---

## Operations

The following operations are provided:
1. Conditions - add conditions on a task
2. For loop - execute a list of tasks in a loop
3. Sleep - sleep execution for the specified duration
4. Asserts - assert a condition. Assert failure stops the execution
5. Publish - publishes a value to the admin server


---

## Extensions

All operations inpput/output parameters on a service are implemented as extensions. And new extensions can be easily built and deployed.

In addition to the opererations, a service definition extension is provided that allows you to define the service url, authentication mechanism with credentials.

---

## Templating

Every set of services has it's own convention and a generic orchestration platform makes the workflow definition look complex. MasterMind allows you to build your own templates that then can be used within the JSON to simplify the worflow.

Template definitions can be provided in a hosted templates JSON or in the task definition JSON. A default templates JSON is provided called mastermind.json that has some generic definitions.

Hosted templates are defined as a "name": "url" format.

Templates have the following attributes:
1. the name of the template
2. the type of the template - an extension with this type needs to have been registered.
3. the value is a set of "name": "action" pairs

Examples of template definitions:
		"hostedTemplates": {
			"mm": "http://host.docker.internal:8002/api/templates/mastermind",
			"osc": "http://host.docker.internal:8002/api/templates/oscv3"
			},
		"templates": [{   
				"name": "loop2",
				"type": "loop",
				"value": {
					"inits": { "${this.input.counter}": "0" },
					"whileCondition": "${this.input.counter} < ${template.params.count}",
					"iterators": { "${this.input.counter}": "${this.input.counter} + 2" }
				}
			}]


---

## A sample JSON definition:

{
	
	"flowId": "simple-flow",
	
	"scenario": {
		"name": "flowSimple",
		"description": "",
		"hostedTemplates": {"osc": "http://host.docker.internal:8002/api/templates/oscv3"},
		"extensions": {"osc.api": {"host": "<hostname>", "username": "", "password": ""}},

		"tasks": [
			{ "GET": "/accounts?limit=1" }
		]
	},
	
	"executionStatusUrl" : "http://host.docker.internal:8002/api/execution"
}

More examples are in the test folder.

