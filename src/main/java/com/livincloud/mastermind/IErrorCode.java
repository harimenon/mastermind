package com.livincloud.mastermind;

/**
 * Error code 
 * 
 * @author hpmenon
 *
 */
public interface IErrorCode {

	public int getCode();
	
	public String getDescription();
}
