package com.livincloud.mastermind.util;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.livincloud.mastermind.InvalidInputException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExpressionHandler {

	private ExpressionParser expressionParser = new SpelExpressionParser();  
	
	private Methods methods = new Methods();
	
	public ExpressionHandler() {
	}
	
	public int intValue(String str) throws InvalidInputException {
		try {
			return Integer.parseInt(value(str).toString());
		}
		catch(NumberFormatException e) {
			throw new InvalidInputException(str, e.getMessage());
		}
	}

	public String stringValue(String str) throws InvalidInputException {
		return value(str).toString();
	}

	public boolean booleanValue(String str) throws InvalidInputException {
		Object result = value(str);
		return (result instanceof Boolean) && (((Boolean)result) == true);
	}

	public Object value(String str) throws InvalidInputException {
		try {
			EvaluationContext context = new StandardEvaluationContext(methods);
			Expression expression = expressionParser.parseExpression(str);
			Object result = expression.getValue(context);
			log.trace("Value: {}", result);
			return result;
		}
		catch(SpelEvaluationException | SpelParseException spelEx) {
			log.debug("Invalid expression: {}", spelEx.getMessage());
			throw new InvalidInputException(str, "Invalid expression: " + spelEx.getMessage());
		}
	}
	
	class Methods {
		public boolean IN(Object value, final Collection<Object> values) {
			return values.contains(value);
		}
		
		public String MERGE(final Collection<Object> values, String separator) {
			return values.stream().map(Object::toString).collect(Collectors.joining(separator));
		}
		
		////////////////
		// String versions to be used if values are Long
		// SPEL doesn't auto promote from Int to Long. So of the REST API
		// returns a long, SPEL fails as there's no 'L' at the end of the
		// Longs that the REST API generates.
		////////////////
		
		public boolean IN(String value, String valuesAsString) {
			String[] values = ListMarshaller.unmarshall(valuesAsString);
			for(int i = 0; i < values.length; ++i) {
				if(value.compareTo(values[i]) == 0)
					return true;
			}
			return false;
		}
		
		public String MERGE(String valuesAsString, String separator) {
			return ListMarshaller.unmarshallAsStr(valuesAsString).replaceAll(",", separator);
		}
	}
}
