package com.livincloud.mastermind.util;

import org.slf4j.Logger;
import org.slf4j.MDC;

public class LogUtils {
	public static void logHeader(Logger log, String header) {
		log.trace( "--------------------------------------------------- " + header );
	}
	
	public static void setContext(String contextId) {
		setContext(contextId, "");
	}
	
	public static void setContext(String contextId, String taskId) {
		MDC.put("mastermind.contextId", contextId);
		MDC.put("mastermind.taskId", taskId);
	}
	
	public static void clearContext() {
		MDC.clear();
	}
}
