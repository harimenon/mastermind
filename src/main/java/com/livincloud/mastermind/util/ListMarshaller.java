package com.livincloud.mastermind.util;

import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ListMarshaller {
	public static String marshall(List<?> list) {
		if(list.size() == 0)
			return "";
		
		if(list.size() == 1) {
			log.trace("Flattening single string: {}", list.get(0));
			return list.get(0).toString();
		}
		
		return list.stream().map(Object::toString).collect(Collectors.joining(",", "{", "}"));
	}
	
	public static String[] unmarshall(String listStr) {
		return unmarshallAsStr(listStr).split(",");
	}
	
	public static String unmarshallAsStr(String listStr) {
		return listStr.replaceAll("\\{", "").replaceAll("\\}", "");
	}
}
