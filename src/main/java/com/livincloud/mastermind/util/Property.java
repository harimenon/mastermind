package com.livincloud.mastermind.util;

import java.util.AbstractMap;

public class Property extends AbstractMap.SimpleEntry<String, String> {

	public Property(String key, String value) {
		super(key, value);
	}

	private static final long serialVersionUID = 1L;
}
