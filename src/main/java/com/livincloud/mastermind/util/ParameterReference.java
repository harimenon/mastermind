package com.livincloud.mastermind.util;

import com.livincloud.mastermind.InvalidInputException;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParameterReference {
	private static final String PARAMETER_TYPE_INPUT = "input";
	private static final String PARAMETER_TYPE_OUTPUT = "output";
	private static final String PARAMETER_SCOPE_GLOBAL = "global";
	private static final String PARAMETER_SCOPE_CURRENTTASK = "this";

	private String parameterScope;
	
	private String parameterType;
	
	private String parameterPath;
	
	public static ParameterReference parse(String paramRefStr) throws InvalidInputException {
		ReferenceToken refToken = ReferenceToken.parse(paramRefStr);
		return parse(refToken);
	}
	
	public static ParameterReference parse(ReferenceToken refToken) throws InvalidInputException {
		PathElementTraverser pathElementTraverser = new PathElementTraverser(refToken.getReferenceTag());
		String scope = pathElementTraverser.currentElement();
		String type = pathElementTraverser.next().currentElement();
		String path = pathElementTraverser.next().next().path();
		
		if(scope.isEmpty())
			throw new InvalidInputException(refToken.getReferenceTag(), "Missing SCOPE parameter.");
		
		if(type.isEmpty())
			throw new InvalidInputException(refToken.getReferenceTag(), "Missing TYPE parameter.");
		
		if(path.isEmpty())
			throw new InvalidInputException(refToken.getReferenceTag(), "Missing PATH parameter.");
		
		return new ParameterReference(scope, type, path);
	}
	
	public boolean isInputParameter() {
		return parameterType.equalsIgnoreCase(PARAMETER_TYPE_INPUT);
	}
	
	public boolean isOutputParameter() {
		return parameterType.equalsIgnoreCase(PARAMETER_TYPE_OUTPUT);
	}
	
	public boolean isGlobalParameter() {
		return parameterScope.equalsIgnoreCase(PARAMETER_SCOPE_GLOBAL);
	}
	
	public boolean isInScope(String taskName) {
		return parameterScope.equalsIgnoreCase(PARAMETER_SCOPE_CURRENTTASK) || parameterScope.equalsIgnoreCase(taskName);
	}
}


