package com.livincloud.mastermind.util;

import java.util.regex.Pattern;

import com.livincloud.mastermind.InvalidInputException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
public class ReferenceToken {
	private static final String TOKEN_START = "${";
	private static final String TOKEN_START_FORK = "#{";
	private static final String TOKEN_END = "}";
	
	private String beforeText;
	private String referenceTag;
	private String afterText;
	private boolean isForkable;
	
	public static boolean isReference(String str) {
		return (str.startsWith(TOKEN_START) || str.startsWith(TOKEN_START_FORK)) && (str.indexOf(TOKEN_END) != -1);
	}
	
	public static ReferenceToken parse(String str) throws InvalidInputException {
		boolean fork = false;
		boolean startTokenFound = false;

		if(str.startsWith(TOKEN_START))
			startTokenFound = true;
		else if(str.startsWith(TOKEN_START_FORK))
			startTokenFound = fork = true;

		if(!startTokenFound) {
			throw new InvalidInputException(str, "Doesn't start with: " + TOKEN_START + " or " + TOKEN_START_FORK);
		}
		
		int endIndex = str.indexOf(TOKEN_END);
		if(endIndex == -1) {
			throw new InvalidInputException(str, "Token end not found.");
		}
		
		String token = str.substring(TOKEN_START.length(), endIndex);
		if(token == null) {
			throw new InvalidInputException(str, "Start index: " + TOKEN_START + "; End index: " + TOKEN_START_FORK);
		}
		
		log.trace("Token found: {}", token);
		
		return new ReferenceToken(
						"", 
						token, 
						str.substring(token.length() + TOKEN_START.length() + TOKEN_END.length()),
						fork);
	}
	
	public static ReferenceToken findFirstIn(String str) throws InvalidInputException {
		int index = -1;
		
		 for(int i=0; i < str.length()-1; i++) {
		     char c = str.charAt(i);
		     if( (str.charAt(i+1) == '{') && ((c == '#') || (c == '$')) ) {
	    		 index = i;
	    		 break;
		     }
		 }
		 
		 if(index == -1) 
			 return null;
		 
		 ReferenceToken tokenFound = parse(str.substring(index));
		 if(index > 0) {
			 tokenFound.beforeText = str.substring(0, index);
		 }
		 return tokenFound;
	}
	
	public String replaceAll(String str, String resolvedValue) {
		return str.replaceAll(Pattern.quote(toString()), resolvedValue);
	}

	@Override
	public String toString() {
		return (isForkable() ? TOKEN_START_FORK : TOKEN_START) + getReferenceTag() + TOKEN_END;
	}
}
