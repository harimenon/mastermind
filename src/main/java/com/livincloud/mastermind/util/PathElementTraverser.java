package com.livincloud.mastermind.util;

public class PathElementTraverser {
	
	private static char PATH_SEPARATOR = '.';
	
	private String originalPath;
	private String currentPath;
	private String currentElement = null;
	
	public PathElementTraverser(String path) {
		this.currentPath = this.originalPath = path;
		setCurrentElement();
	}
	
	private PathElementTraverser(String path, String originalPath) {
		this.currentPath = path;
		this.originalPath = path;
		setCurrentElement();
	}
	
	public String currentElement() {
		if(currentElement == null)
			setCurrentElement();
		return currentElement;
	}
	
	public boolean hasMore() {
		return currentElement.length() < currentPath.length();
	}
	
	public PathElementTraverser next() {
		String path = (currentElement.length() + 1 < currentPath.length()) ? currentPath.substring(currentElement.length() + 1) : "";
		return new PathElementTraverser(path, originalPath);
	}
	
	public String path() {
		return currentPath;
	}
	
	public String originalPath() {
		return originalPath;
	}
	
	private void setCurrentElement() {
		currentElement = currentPath;
		int endIndex = currentPath.indexOf(PATH_SEPARATOR);
		if(endIndex != -1)
			currentElement = currentPath.substring(0, endIndex);
	}
}
