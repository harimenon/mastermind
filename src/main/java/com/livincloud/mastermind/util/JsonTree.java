package com.livincloud.mastermind.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonTree {
	protected LinkedTreeMap<?,?> jsonMap;
	protected JsonParser jsonParser = new JsonParser();
	
	public JsonTree(LinkedTreeMap<?,?> jsonMap) {
		this.jsonMap = jsonMap;
	}
	
	public JsonTree clone() {
		return new JsonTree(jsonParser.duplicate(jsonMap));
	}
	
	public String toString() {
		return jsonMap.toString();
	}
	
	public String getString(String name) {
		return getChild(name, String.class);
	}
	
	public JsonTree getChildTree(String name) {
		LinkedTreeMap<?,?> child = getChild(name, LinkedTreeMap.class);
		return child == null ? null : new JsonTree(child);
	}
	
	public List<?> getChildList(String name) {
		return getChild(name, List.class);
	}
	
	public Object removeChild(String name) {
		return jsonMap.remove(name);
	}
	
	public int size() {
		return jsonMap.size();
	}
	
	public Set<?> keySet() {
		return jsonMap.keySet();
	}
	
	public Collection<?> values() {
		return jsonMap.values();
	}
	
	public List<Object> find(String searchPath) throws InvalidInputException {
		PathElementTraverser searchPathTraverser = new PathElementTraverser(searchPath);
		List<Object> results = find(searchPathTraverser, jsonMap);
		return (results != null) ? results : new ArrayList<Object>();
	}
	
	@SuppressWarnings("unchecked")
	private <T> T getChild(String name, Class<T> type) {
		Object child = jsonMap.get(name);
		log.trace("Child type [{}]: {}", name, (child == null) ? "NULL" : child.getClass());
		return (child != null) && type.isInstance(child) ? (T) child : null;
	}
	
	private List<Object> find(PathElementTraverser searchPathTraverser, LinkedTreeMap<?,?> currentNode) throws InvalidInputException {
		log.trace("Current search path: {}", searchPathTraverser.path());
		
		if(searchPathTraverser.currentElement().isEmpty())
			throw new InvalidInputException(searchPathTraverser.originalPath(), "Path not resolved: " + searchPathTraverser.originalPath());
		
		Object o = currentNode.get(searchPathTraverser.currentElement());
		if(o == null)
			throw new InvalidInputException(searchPathTraverser.originalPath(), "Path element not resolved: " + searchPathTraverser.currentElement());
			
		return findItem(searchPathTraverser, o);
	}
	
	@SuppressWarnings("unchecked")
	private List<Object> findItem(PathElementTraverser searchPathTraverser, Object o) throws InvalidInputException {
		log.trace("Token [{}]: {}", searchPathTraverser.currentElement(), o.getClass().getName());

		if(o instanceof LinkedTreeMap) {
			return searchPathTraverser.hasMore() ? find(searchPathTraverser.next(), (LinkedTreeMap<?,?>) o) : new ArrayList<>(Arrays.asList(o));
		}
		
		List<Object> results = new ArrayList<Object>();
		if(o instanceof List) {
			if(searchPathTraverser.hasMore()) {
				for(Object listItem : ((List<?>)o))
					results.addAll(findItem(searchPathTraverser, listItem));
			}
			else {
				return ((List<Object>)o);
			}
		}
		else {
			log.trace("Found value: {}", o.toString());
			results.add(o.toString());
		}
		log.trace("Results count: {}", results.size());
		return results;
	}
	
	public static class Node {
		private Object nodeObject;
		
		public Node(Object nodeObject) {
			this.nodeObject = nodeObject;
		}
		
		@Override
		public String toString() {
			return nodeObject.toString();
		}
		
		public List<Object> search(String searchPath) throws InvalidInputException {
			if(nodeObject instanceof List) {
				List<?> list = ((List<?>)nodeObject);
				return list.stream().map(o -> o.toString()).collect(Collectors.toList());
			}
			else if(nodeObject instanceof LinkedTreeMap) {
				LinkedTreeMap<?,?> map = ((LinkedTreeMap<?,?>)nodeObject);
				JsonTree tree = new JsonTree(map);
				return tree.find(searchPath);
			}
			return new ArrayList<>(Arrays.asList(nodeObject));
		}
	}
}
