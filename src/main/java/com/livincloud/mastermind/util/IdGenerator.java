package com.livincloud.mastermind.util;

import java.util.Random;
import java.util.UUID;

public class IdGenerator {
	
	private static Random random = new Random();
	
	public static String generateGuid() {
		return UUID.randomUUID().toString();
	}
	
	public static String generateId() {
		return String.valueOf(random.nextInt(99999));
	}
}
