package com.livincloud.mastermind.util;

import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateParameterReference {
	private static final String TEMPLATE_PARAMS_TAG = "template.params.";
	
	public static String resolveParameter(String str, String paramName, String paramValue) {
		log.trace("Updating: {} => name={}/value={}", str, paramName, paramValue);
		return str.replaceAll(Pattern.quote("${" + TEMPLATE_PARAMS_TAG + paramName + "}"), paramValue);
	}
	
	public static void resolveParameter(Properties props, String paramName, String paramValue) {
		log.trace("Updating: {} => name={}/value={}", props, paramName, paramValue);
		Set<Entry<Object, Object>> entries = props.entrySet();
		for (Entry<Object, Object> entry : entries) {
			props.setProperty(entry.getKey().toString(), resolveParameter(entry.getValue().toString(), paramName, paramValue));
	    }
		log.trace("Updated: {}", props);
	}
}
