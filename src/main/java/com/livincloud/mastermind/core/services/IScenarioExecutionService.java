package com.livincloud.mastermind.core.services;

import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.core.execution.IScenarioExecutionStatusListener;

public interface IScenarioExecutionService {
	void execute(Scenario scenario, IScenarioExecutionStatusListener executionStatusCallback);
}
