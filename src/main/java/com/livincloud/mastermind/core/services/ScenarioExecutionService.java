package com.livincloud.mastermind.core.services;

import org.springframework.stereotype.Service;

import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.IScenarioExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ScenarioExecutor;

@Service
public class ScenarioExecutionService implements IScenarioExecutionService {
	
	@Override
	public void execute(Scenario scenario, IScenarioExecutionStatusListener executionStatusCallback) {
	
		ExecutionContext ctx = new ExecutionContext(scenario.getName(), scenario.getInputParameters());
		
		ScenarioExecutor scenarioExecutor = new ScenarioExecutor(scenario, executionStatusCallback);
		scenarioExecutor.execute(ctx);
	}
}
