package com.livincloud.mastermind.core.execution;

import java.util.HashMap;
import java.util.Map;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.util.LogUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class for all Task Executors
 * 
 * @author hpmenon
 *
 */
@Slf4j
public abstract class TaskExecutor implements ITaskExecutor {
	private Task task;
	private ITaskExecutionStatusListener executionStatusListener;
	Map<String, String> taskInputParameters = new HashMap<String, String>();
	
	public TaskExecutor(Task task, ITaskExecutionStatusListener executionStatusListener) {
		this.task = task;
		this.executionStatusListener = executionStatusListener;
	}
	
	protected Task getTask() {
		return task;
	}
	
	protected ITaskExecutionStatusListener getTaskExecutionStatusListener() {
		return executionStatusListener;
	}

	@Override
	public boolean execute(ExecutionContext ctx) {
		LogUtils.logHeader(log, "Executing task [" + ctx.getId() + "]: " + task.getName());
		
		log.trace("Task execution START: {}", task.getName());
		
		// Create execution instance
		ITaskInstance taskInstance = createExecutionInstance(ctx);
		
		try {
			ctx.pushTask(taskInstance);
			executionStatusListener.onExecutionStart(ctx, taskInstance);
			
			// Before execution
			boolean continueOn = taskInstance.beforeExecution();
			
			// Execute
			if(continueOn) {
				if(!taskInstance.execute()) {
					throw new TaskExecutionException(task.getName(), "Task execution failed");
				}
				
				// execute child tasks
				if(!executeChildTasks(taskInstance)) {
					throw new TaskExecutionException(task.getName(), "Child tasks execution failed");
				}
				
				// After execution
				taskInstance.afterExecution();
			}
			
			log.trace("Task execution END: {}", task.getName());
			executionStatusListener.onExecutionSuccess(ctx, taskInstance);
			return true;
		}
		catch (TaskExecutionException e) {
			log.error("Task execution error: {}", e.getMessage());
			executionStatusListener.onExecutionFailure(ctx, taskInstance, e);
		}
		finally {
			ctx.popTask();
		}
		return false;
	}
	
	protected boolean executeChildTasks(ITaskInstance taskInstance) throws TaskExecutionException {
		if(task.hasChildTasks()) {
			// Child tasks executed in the context of the task instance
			// i.e. each task type decides when/how to execute the child tasks
			return taskInstance.executeChildTasks(new TaskListExecutor(task.getChildTasks(), executionStatusListener));
		}
		
		return true;
	}
	
	protected abstract ITaskInstance createExecutionInstance(ExecutionContext ctx);
}
