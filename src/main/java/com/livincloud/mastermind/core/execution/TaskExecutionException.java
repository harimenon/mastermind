package com.livincloud.mastermind.core.execution;

import com.livincloud.mastermind.InvalidInputException;

import lombok.Getter;

public class TaskExecutionException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	@Getter private String taskName;

	public TaskExecutionException(String taskName, String message) {
		super("Task failed [" + taskName + "]: " + message);
		
		this.taskName = taskName;
	}

	public TaskExecutionException(InvalidInputException e) {
		super(e.getDisplayMessage());
	}

	public TaskExecutionException(String message) {
		super(message);
	}
}
