package com.livincloud.mastermind.core.execution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.util.ExpressionHandler;
import com.livincloud.mastermind.util.ListMarshaller;
import com.livincloud.mastermind.util.ParameterReference;
import com.livincloud.mastermind.util.ReferenceToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskReferenceResolver {
	private ExecutionContext ctx;
	private String taskName;
	private Map<String, String> taskInputParameters;
	
	private ExpressionHandler expressionHandler = new ExpressionHandler(); 
	
	public TaskReferenceResolver(String taskName, ExecutionContext ctx, Map<String, String> taskInputParameters) {
		this.ctx = ctx;
		this.taskName = taskName;
		this.taskInputParameters = taskInputParameters;
	}
	
	public String getTaskName() {
		return taskName;
	}
	
	public List<ISearchable> resolveRuntimeReferences(String str) throws InvalidInputException {
		return resolveReferences(str, false);
	}
	
	public String resolveRuntimeReferencesAsString(String str) throws InvalidInputException {
		List<?> list = resolveReferences(str, true);
		return (list.size() > 0) ? list.get(0).toString() : "";
	}

	public List<ISearchable> findReferenceValues(String forkingRef) throws InvalidInputException {
		return getReferencedValues(ReferenceToken.findFirstIn(forkingRef));
	}

	public void updateInputParameter(String name, String value) throws InvalidInputException {
		log.trace("Adding task inits: {}={}", name, value);
		taskInputParameters.put(name,  value);
	}

	public void updateInputParameters(Properties params) throws InvalidInputException {
		log.trace("Evaluating task inits: {}", params);
		for (Entry<Object, Object> entry : params.entrySet()) {
			updateInputParameter(
					Task.getInputParameterName(entry.getKey().toString()), 
					expressionHandler.stringValue(resolveRuntimeReferencesAsString(entry.getValue().toString())));
	    }
	}
	
	private List<ISearchable> resolveReferences(String str, boolean singleString) throws InvalidInputException {
		List<ISearchable> resolvedValues = new ArrayList<ISearchable>();
		
		log.trace("RESOLVING REF [{}]: {}", singleString, str);

		ReferenceToken token = ReferenceToken.findFirstIn(str);
		if(token == null) {
			log.trace("No references in string: {}", str);
			resolvedValues.add(new ISearchable.SearchableString(str));
			return resolvedValues;
		}
		
		List<ISearchable> resolvedValuesInToken = getReferencedValues(token);
		log.trace("ResolvedTokens count: {}", resolvedValuesInToken.size());
		
		if(resolvedValuesInToken.size() > 0) {
			if(token.isForkable() && !singleString) {
				for(Object resolvedValue : resolvedValuesInToken) {
					log.trace("Resolved value [{}]: {}", token.getReferenceTag(), resolvedValue.toString());
					resolvedValues.addAll(resolveReferences(token.replaceAll(str, resolvedValue.toString()), singleString));
				}
			}
			else {
				String resolvedValue = ListMarshaller.marshall(resolvedValuesInToken);
				log.trace("Resolved string value [{}]: {}", token.getReferenceTag(), resolvedValue.toString());
				resolvedValues.addAll(resolveReferences(token.replaceAll(str, resolvedValue.toString()), singleString));
			}
		}

		return resolvedValues;
	}
	
	private List<ISearchable> getReferencedValues(ReferenceToken token) throws InvalidInputException {
		
		log.trace("Searching for: {}", token);
		
		ParameterReference parameterReference = ParameterReference.parse(token);
		
		if(parameterReference.isOutputParameter()) {
			// If current task (by name or using 'this') then use this taskname otherwise use given task name
			// (helps support 'this' in unnamed tasks)
			String scope = parameterReference.isInScope(taskName) ? taskName : parameterReference.getParameterScope();
			
			// Look in global results map
			log.trace("Searching in task outputs: {}.{}", scope, parameterReference.getParameterPath());
			return ctx.searchTaskOutputs(scope, parameterReference.getParameterPath());
		}
		else if(parameterReference.isInputParameter()) {
			// Look in task local input parameters
			if(parameterReference.isInScope(taskName)) {
				log.trace("Searching in task inputs: {}.{}", taskName, parameterReference.getParameterPath());
				String inputParam = getTaskInputParameterValue(parameterReference.getParameterPath());
				if(inputParam != null)
					return new ArrayList<>(Arrays.asList(new ISearchable.SearchableString(inputParam)));
			}
			
			// Look in global parameters
			log.trace("Searching in global inputs: {}", parameterReference.getParameterPath());
			return ctx.searchInputParameters(parameterReference.getParameterPath());
		}

		throw new InvalidInputException(token.getReferenceTag(), "Parameter type should be 'input'/'output'");
	}
	
	private String getTaskInputParameterValue(String name) throws InvalidInputException {
		String value = taskInputParameters.get(name);
		if(value == null)
			return null;
		
		log.trace("Found Task input parameter [{}] = {}", name, value);
		return value;
	}
}
