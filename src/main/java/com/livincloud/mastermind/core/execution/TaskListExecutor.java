package com.livincloud.mastermind.core.execution;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskList;
import com.livincloud.mastermind.util.LogUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskListExecutor {
	private TaskList taskList;
	private ITaskExecutionStatusListener executionStatusListener;
	
	public TaskListExecutor(TaskList taskList, ITaskExecutionStatusListener executionStatusListener) {
		this.taskList = taskList;
		this.executionStatusListener = executionStatusListener;
	}
	
	/**
	 * Executes the tasks
	 * 
	 * @param ctx
	 */
	public boolean execute(ExecutionContext ctx) {
		log.trace("Starting task list execution");
		return taskList.isEmpty() ? true : executeTasks(ctx);
	}
	
	private boolean executeTasks(ExecutionContext ctx) {
		if(taskList.shouldParallelizeExecution())
			log.debug("Parallel task execution enabled");

		return Executor.execute(ctx, taskList.getTasks(), taskList.shouldParallelizeExecution(), new Executor.IExecutionCallback<Task>() {
			@Override
			public boolean execute(ExecutionContext ctx, Task task) {
				LogUtils.logHeader(log, "Executing task [" + ctx.getId() + "]: " + task.getName());
				ITaskExecutor taskExecutor = task.getTaskExecutor(executionStatusListener);
				return taskExecutor.execute(ctx);
			}
		});
	}
}
