package com.livincloud.mastermind.core.execution;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.ITaskOutput;
import com.livincloud.mastermind.util.IdGenerator;
import com.livincloud.mastermind.util.LogUtils;
import com.livincloud.mastermind.util.PathElementTraverser;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * The context carried across a single execution thread
 * 
 * @author hpmenon
 *
 */
@Slf4j
public class ExecutionContext {
	
	private static final String FORK_ID_PREFIX = "fork-";

	/*
	 * Id for this execution context
	 */
	@Getter private String id;
	
	/*
	 * Input parameters for the execution
	 */
	@Getter private Map<String, ISearchable> inputParameters = new HashMap<String, ISearchable>();
	
	/*
	 * Output of each task
	 */
	private Map<String, ITaskOutput> outputMap = new HashMap<String, ITaskOutput>();
		
	/*
	 * Results for each task
	 */
	private Map<String, Map<String, Object>> resultsMap = new HashMap<String, Map<String, Object>>();
	
	/*
	 * Current call stack
	 */
	private Stack<ITaskInstance> callStack = new Stack<ITaskInstance>();

	
	/**
	 * Constructs the execution context object
	 * 
	 * @param inputParameters
	 */
	public ExecutionContext(String scenarioName, Map<String, ISearchable> inputParameters) {
		this.inputParameters = inputParameters;
		this.id = scenarioName + IdGenerator.generateId();
	}
	
	// Private constructor
	private ExecutionContext(String id) {
		this.id = id;
	}
	
	/**
	 * Clones this execution context for parallel task execution.
	 * 
	 * @param forkId - id to differentiate the different forks
	 * @return cloned ExecutionContext
	 */
	public ExecutionContext clone(int forkId) {
		log.trace("Cloning context: {}/fork-{}", id, forkId);
		
		ExecutionContext clonedCtx = new ExecutionContext(id + "/" + FORK_ID_PREFIX + forkId);
		
		// For now shallow clone - can't think of a case where individual map elements would be
		// modified across tasks. All these are task specific.
		clonedCtx.inputParameters = new HashMap<String, ISearchable>(inputParameters);
		clonedCtx.outputMap = new HashMap<String, ITaskOutput>(outputMap);
		clonedCtx.resultsMap = new HashMap<String, Map<String, Object>>(resultsMap);
		return clonedCtx;
	}
	
	/**
	 * Current task
	 *  
	 * @param currentTask
	 */
	public void pushTask(ITaskInstance currentTask) {
		callStack.push(currentTask);
		LogUtils.setContext(getId(), currentTask.getTaskName());
	}
	
	public void popTask() {
		callStack.pop();
	}
	
	/**
	 * Returns the string representation of the callstack
	 * 
	 * @return call stack as a string
	 */
	public String getCallStack() {
		return callStack.stream().map(ITaskInstance::getTaskName).collect(Collectors.joining("/"));
	}
	
	public ExecutionContext addTaskOutput(String taskName, ITaskOutput taskOutput) {
		outputMap.put(taskName, taskOutput);
		return this;
	}
	
	public ITaskOutput getTaskOutput(String taskName) {
		return outputMap.get(taskName);
	}
	
	public ExecutionContext addResults(String taskName, String resultName, Object value) {
		Map<String, Object> results = resultsMap.get(taskName);
		if(results == null) {
			results = new HashMap<String, Object>();
			resultsMap.put(taskName, results);
		}
		log.trace("Adding result: {} = {}", resultName, value);
		results.put(resultName, value);
		return this;
	}
	
	public Map<String, Object> getResultsMap(String taskName) {
		Map<String, Object> results = resultsMap.get(taskName);
		if(results == null)
			results = new HashMap<String, Object>();
		return Collections.unmodifiableMap(results);
	}

	public ExecutionContext addInputParameter(String name, ISearchable value) {
		log.trace("Adding input param: {} = {}", name, value);
		inputParameters.put(name, value);
		return this;
	}

	/**
	 * Search in task outputs
	 * 
	 * @param taskName
	 * @param searchPath
	 * @return list of results - each is in-turn searchable
	 * @throws InvalidInputException
	 */
	public List<ISearchable> searchTaskOutputs(String taskName, String searchPath) throws InvalidInputException {
		ITaskOutput taskOutput = outputMap.get(taskName);
		if(taskOutput == null)
			throw new InvalidInputException(searchPath, "Output parameter not found for task: " + taskName);
		return taskOutput.search(searchPath);
	}
	
	/**
	 * Search in input parameters
	 * 
	 * @param path
	 * @return list of results - each is in-turn searchable
	 * @throws InvalidInputException
	 */
	public List<ISearchable> searchInputParameters(String path) throws InvalidInputException {
		// If the path has multiple elements then the first element is the name
		// of the input parameter.
		PathElementTraverser pathElementTraverser = new PathElementTraverser(path);
		String paramName = pathElementTraverser.currentElement();
		String searchPath = pathElementTraverser.next().path(); // could be empty if path has only 1 element. e.g. when input parameter is not just a string

		log.trace("Searching for input param: {}/{}", paramName, searchPath);
		ISearchable inputParam = inputParameters.get(paramName);
		if(inputParam == null)
			throw new InvalidInputException(path, "Input parameter not found.");
		log.trace("Input param found: {}:{}", inputParam.getClass().getName(), inputParam.toString());
		return inputParam.search(searchPath);
	}
}
