package com.livincloud.mastermind.core.execution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.ITaskOutput;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.util.ExpressionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class TaskInstance implements ITaskInstance {
	
	protected Task task;
	protected ExecutionContext ctx;

	private Map<String, String> taskInputParameters = new HashMap<String, String>();
	private long executionTime = 0;

	protected ExpressionHandler expressionHandler = new ExpressionHandler(); 
	
	protected TaskReferenceResolver taskReferenceResolver;
	
	protected TaskInstance(Task task, ExecutionContext ctx) {
		this.task = task;
		this.ctx = ctx;
		this.taskReferenceResolver = new TaskReferenceResolver(task.getName(), ctx, taskInputParameters);
	}
	
	@Override
	public String getTaskName() {
		return task.getName();
	}
	
	protected ExecutionContext getExecutionContext() {
		return ctx;
	}

	@Override
	public boolean beforeExecution() throws TaskExecutionException {
		// ***
		// Run the inits
		// ***
		try {
			if(task.getInits() != null) {
				log.trace("Evaluating task inits: " + task.getInits());
					updateInputParameters(task.getInits());
			}
		} 
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
		
		// ***
		// Invoke the task extensions
		// ***
		boolean continueOn = true;
		for(TaskExtension taskExtension : task.getExtensions()) {
			if(!(continueOn = taskExtension.beforeTaskExecution(this, ctx)))
				break;
		}
		
		return continueOn;
	}
	
	@Override
	public boolean execute() throws TaskExecutionException {
		boolean continueOn = true;
		
		// ***
		// Invoke the task extensions
		// ***
		for(TaskExtension taskExtension : task.getExtensions()) {
			if(!(continueOn = taskExtension.withTaskExecution(this, ctx)))
				break;
		}
		
		return continueOn;
	}
	
	@Override
	public boolean executeChildTasks(TaskListExecutor childTaskExecutor) throws TaskExecutionException {
		return childTaskExecutor.execute(ctx);
	}
	
	@Override
	public boolean afterExecution() throws TaskExecutionException {
		// ***
		// Invoke the task extensions
		// ***
		boolean continueOn = true;
		for(TaskExtension taskExtension : task.getExtensions()) {
			if(!(continueOn = taskExtension.afterTaskExecution(this, ctx)))
				break;
		}

		// Update the execution time 
		addTaskResults("executionTime", executionTime);
		
		return continueOn;
	}
	
	@Override
	public void updateInputParameter(String name, String value) throws InvalidInputException {
		taskReferenceResolver.updateInputParameter(name, value);
	}
	
	@Override
	public void updateInputParameters(Properties params) throws InvalidInputException {
		taskReferenceResolver.updateInputParameters(params);
	}

	@Override
	public String resolveRuntimeReferencesAsString(String str) throws InvalidInputException {
		return taskReferenceResolver.resolveRuntimeReferencesAsString(str);
	}

	@Override
	public List<ISearchable> resolveRuntimeReferences(String str) throws InvalidInputException {
		return taskReferenceResolver.resolveRuntimeReferences(str);
	}
	
	protected void addTaskOutput(ITaskOutput taskOutput) {
		this.getExecutionContext().addTaskOutput(getTaskName(), taskOutput);
	}
	
	protected void addTaskResults(String resultName, Object value) {
		this.getExecutionContext().addResults(getTaskName(), resultName, value);
	}
	
	protected Timer startTimer() {
		return new Timer();
	}
	
	protected void registerExecutionTime(long endTime) {
		executionTime = endTime;
	}

	protected class Timer {
		long startTime;
		
		public Timer() {
			startTime = System.currentTimeMillis();
		}
		
		public long end() {
			return System.currentTimeMillis() - startTime;
		}
	}
}
