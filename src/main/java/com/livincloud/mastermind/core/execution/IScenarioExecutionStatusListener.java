package com.livincloud.mastermind.core.execution;

/**
 * Callback for scenario execution events
 * 
 * @author hpmenon
 *
 */
public interface IScenarioExecutionStatusListener extends ITaskExecutionStatusListener {
	
	void onExecutionStart(ExecutionContext ctx);
	
	void onExecutionEnd(ExecutionContext ctx, boolean success);
}
