package com.livincloud.mastermind.core.execution;

import com.livincloud.mastermind.core.domain.Scenario;

/**
 * Executes a given scenario
 * 
 * @author hpmenon
 *
 */
public class ScenarioExecutor {
	private Scenario scenario;
	private IScenarioExecutionStatusListener executionStatusListener;
	
	public ScenarioExecutor(Scenario scenario,  IScenarioExecutionStatusListener executionStatusListener) {
		this.scenario = scenario;
		this.executionStatusListener = executionStatusListener;
	}
	
	/**
	 * Executes the scenario
	 * 
	 * @param ctx
	 */
	public void execute(ExecutionContext ctx) {
		executionStatusListener.onExecutionStart(ctx);
		
		TaskListExecutor taskListExecutor = new TaskListExecutor(
				scenario.getTaskList(),
				executionStatusListener);
		boolean success = taskListExecutor.execute(ctx);
		
		executionStatusListener.onExecutionEnd(ctx, success);
	}
}
