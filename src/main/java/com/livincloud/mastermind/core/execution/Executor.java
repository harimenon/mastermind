package com.livincloud.mastermind.core.execution;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Managers serial and parallel execution
 * 
 * @author hari
 *
 */
public class Executor {
	private static final int MAX_PARALLEL_TASKS = 16;

	/**
	 * Executes the given execution callback for all elements in the list
	 *  
	 * @param ctx - the context to pass to the execution callback. Replicates the context in case of parallel execution; reuses the same context for serial execution.
	 * @param list - the list of parameters to be passed to the execution callback
	 * @param isParallelExecution - true for parallel; false for serial
	 * @param executor - the callback
	 * @return
	 */
	public static <T> boolean execute(ExecutionContext ctx, List<T> list, boolean isParallelExecution, IExecutionCallback<T> executor) {
		if(isParallelExecution) {
			return executeParallel(ctx, list, executor);
		}

		return executeSequential(ctx, list, executor);
	}
	
	// parallel execution
	private static <T> boolean executeParallel(ExecutionContext ctx, List<T> list, IExecutionCallback<T> executor) {
		AtomicInteger index = new AtomicInteger();
		ForkJoinPool forkJoinPool = new ForkJoinPool(Math.max(list.size(), MAX_PARALLEL_TASKS));
		try {
			return forkJoinPool.submit(() -> list.parallelStream().map(e -> executor.execute(ctx.clone(index.incrementAndGet()), e)).allMatch(success -> (success == true))).get();
		}
		catch (InterruptedException | ExecutionException e) {
		}
		return false;
	}
	
	// serial execution
	private static <T> boolean executeSequential(ExecutionContext ctx, List<T> list, IExecutionCallback<T> executor) {
		return list.stream().map(e -> executor.execute(ctx, e)).allMatch(success -> (success == true));
	}
	
	/**
	 * The interface for the execution callback
	 * 
	 * @author hari
	 *
	 * @param <T>
	 */
	public interface IExecutionCallback<T> {
		
		/**
		 * Execution callback method
		 * 
		 * @param ctx - the execution context
		 * @param t - the execution parameter
		 * @return
		 */
		boolean execute(ExecutionContext ctx, T t);
	}
}
