package com.livincloud.mastermind.core.execution;

import java.util.List;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;

/**
 * Runtime instance of a task. 
 * 
 * @author hpmenon
 *
 */
public interface ITaskInstance {

	String getTaskName();
	
	boolean beforeExecution() throws TaskExecutionException;
	
	boolean execute() throws TaskExecutionException;
	
	boolean executeChildTasks(TaskListExecutor childTaskExecutor) throws TaskExecutionException;
	
	boolean afterExecution() throws TaskExecutionException;

	void updateInputParameter(String name, String value) throws InvalidInputException;

	void updateInputParameters(Properties params) throws InvalidInputException;
	
	List<ISearchable> resolveRuntimeReferences(String str) throws InvalidInputException;
	
	String resolveRuntimeReferencesAsString(String str) throws InvalidInputException;
}
