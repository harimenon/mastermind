package com.livincloud.mastermind.core.execution;

/**
 * Callback for scenario execution events
 *  
 * @author hpmenon
 *
 */
public interface ITaskExecutionStatusListener {
	
	void onExecutionStart(ExecutionContext ctx, ITaskInstance ti);
	
	void onExecutionSuccess(ExecutionContext ctx, ITaskInstance ti);

	void onExecutionFailure(ExecutionContext ctx, ITaskInstance ti, TaskExecutionException taskExecutionException);
}
