package com.livincloud.mastermind.core.execution;

/**
 * Executor for a task. An executor is created for each task
 * to be executed.
 * 
 * @author hpmenon
 *
 */
public interface ITaskExecutor {
	boolean execute(ExecutionContext ctx); 
}
