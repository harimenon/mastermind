package com.livincloud.mastermind.core.domain;

import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;

import lombok.Getter;

/**
 * Task extension - allows pre/post-processing
 * 
 * @author hpmenon
 *
 */
public abstract class TaskExtension {
	@Getter private String name;
	
	public TaskExtension(String name) {
		this.name = name;
	}
	
	public boolean beforeTaskExecution(ITaskInstance ti, ExecutionContext ctx) throws TaskExecutionException {
		return true;
	}
	
	public boolean withTaskExecution(ITaskInstance ti, ExecutionContext ctx) throws TaskExecutionException {
		return true;
	}
	
	public boolean afterTaskExecution(ITaskInstance ti, ExecutionContext ctx) throws TaskExecutionException {
		return true;
	}
}
