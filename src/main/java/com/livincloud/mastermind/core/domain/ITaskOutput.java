package com.livincloud.mastermind.core.domain;

/**
 * Definition of a task output. Allows for operations on the task output, 
 * like search, fork.
 * 
 * @author hpmenon
 *
 */
public interface ITaskOutput extends ISearchable {
}
