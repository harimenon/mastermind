package com.livincloud.mastermind.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.util.JsonTree;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Searchable entity
 * 
 * @author hpmenon
 *
 */
public interface ISearchable {
	List<ISearchable> search(String searchPath) throws InvalidInputException;
	
	public class SearchableString implements ISearchable, Serializable {
		private static final long serialVersionUID = -8334901152094999393L;
		@Getter String value;
		
		public SearchableString(String s) {
			this.value = s;
		}
		
		@Override
		public List<ISearchable> search(String searchPath) throws InvalidInputException {
			return new ArrayList<>(Arrays.asList(new SearchableString(value)));
		}
		@Override
		public String toString() {
			return value;
		}
	}
	
	@Slf4j
	public class SearchableList implements ISearchable, Serializable {
		private static final long serialVersionUID = 6788057079097987976L;
		@Getter List<Object> list;

		public SearchableList(List<Object> list) {
			this.list = list;
		}
		
		@Override
		public List<ISearchable> search(String searchPath) throws InvalidInputException {
			log.trace("Returning list items: {}", list.size());
			return list.stream().map(e -> SearchableJsonTreeNode.map(e)).collect(Collectors.toList());
		}
	}
	
	@Slf4j
	public class SearchableJsonTreeNode implements ISearchable, Serializable {
		private static final long serialVersionUID = 3369127154500497489L;
		@Getter JsonTree.Node value;
		
		public SearchableJsonTreeNode(Object nodeObject) {
			value = new JsonTree.Node(nodeObject);
		}
		
		@Override
		public List<ISearchable> search(String searchPath) throws InvalidInputException {
			return mapList(value.search(searchPath));
		}
		
		@Override
		public String toString() {
			return value.toString();
		}
		
		public static List<ISearchable> mapList(List<Object> list) {
			return list.stream().map(e -> map(e)).collect(Collectors.toList());
		}
		
		@SuppressWarnings("unchecked")
		public static ISearchable map(Object o) {
			log.trace("Mapping object: {}:{}", o.getClass().getName(), o.toString());
			if(o instanceof LinkedTreeMap) {
				return new ISearchable.SearchableJsonTreeNode(o);
			}
			else if(o instanceof List) {
				return new ISearchable.SearchableList((List<Object>)o);
			}
			return new ISearchable.SearchableString(o.toString());
		}
	}
}
