package com.livincloud.mastermind.core.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskList {
	private List<Task> tasks = new ArrayList<Task>();
	private boolean parallelExecution;
	
	public TaskList(List<Task> tasks, boolean parallelExecution) {
		this.tasks = tasks;
		this.parallelExecution = parallelExecution;
	}
	
	public TaskList(boolean parallelizeExecution) {
		this.parallelExecution = parallelizeExecution;
	}
	
	public TaskList addTask(Task task) {
		tasks.add(task);
		return this;
	}
	
	public boolean isEmpty() {
		return (tasks == null) || tasks.isEmpty();
	}
	
	public boolean shouldParallelizeExecution() {
		return parallelExecution;
	}
	
	public List<Task> getTasks() {
		return Collections.unmodifiableList(tasks);
	}
}
