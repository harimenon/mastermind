package com.livincloud.mastermind.core.domain;

import lombok.Data;

@Data
public class UserGroup {

	private String name;

	private int distribution = 100;

	private int intervalBetweenCalls = 0;

	private String geoPop;
}
