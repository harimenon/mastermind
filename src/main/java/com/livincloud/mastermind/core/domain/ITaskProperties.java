package com.livincloud.mastermind.core.domain;

import java.util.Properties;
import java.util.Set;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.util.Property;

/**
 * Task properties
 * 
 * @author hpmenon
 *
 */
public interface ITaskProperties {
	int size();
	
	Set<?> getPropertyNames();
	
	String extractTaskType() throws InvalidInputException;
	
	String extractTaskName() throws InvalidInputException;
	
	String extractTaskDescription() throws InvalidInputException;
	
	Properties extractTaskInitializers() throws InvalidInputException;
	
	TaskList extractChildTasks() throws ServiceException, InvalidInputException;
	
	/**
	 * Returns the String after removing it
	 * 
	 * @param name
	 * @return
	 * @throws InvalidInputException
	 */
	String extractString(String name) throws InvalidInputException;

	/**
	 * Returns the Properties after removing it
	 * 
	 * @param name
	 * @return
	 * @throws InvalidInputException
	 */
	Properties extractProperties(String name, boolean nullable) throws InvalidInputException;

	/**
	 * Returns the Property after removing it
	 * 
	 * @param name
	 * @return
	 * @throws InvalidInputException
	 */
	Property extractProperty(String name, boolean nullable) throws InvalidInputException;
	
	Properties getProperties(String name, boolean nullable) throws InvalidInputException;
}
