package com.livincloud.mastermind.core.domain;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.server.ServiceException;

/**
 * Factory that can create tasks
 * 
 * @author hpmenon
 *
 */
public interface ITaskFactory {
	Task createTask(ITaskProperties taskProperties) throws InvalidInputException, ServiceException;
}
