package com.livincloud.mastermind.core.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskExecutor;
import com.livincloud.mastermind.util.IdGenerator;
import com.livincloud.mastermind.util.ParameterReference;

import lombok.Getter;

/**
 * The base class for tasks  
 *  
 * @author hpmenon
 *
 */
public abstract class Task {
	
	/*
	 * Unique task name
	 */
	@Getter private final String name;
	
	/*
	 * Task description
	 */
	@Getter private final String description;
	
	/*
	 * inits that will be run before the task executes
	 */
	@Getter private Properties inits;
	
	/*
	 * extensions can be added to extend the task functionality
	 */
	@Getter private List<TaskExtension> extensions = new ArrayList<TaskExtension>();
	
	/*
	 * child tasks
	 */
	private TaskList childTasks;
	
	/*
	 * parallel execution of subtasks?
	 */
	
	/**
	 * Creates the task
	 * 
	 * @param name
	 * @param description
	 * @param inits
	 */
	public Task(String name, String description, Properties inits) {
		this.name = (name == null) ? this.getClass().getSimpleName() + "-" + IdGenerator.generateId() : name;
		this.description = description;
		this.inits = inits;
		this.childTasks = null;
	}
	
	/**
	 * Creates the task
	 * 
	 * @param name
	 * @param description
	 * @param inits
	 */
	public Task(String name, String description, Properties inits, TaskList childTasks) {
		this.name = (name == null) ? this.getClass().getSimpleName() + "-" + IdGenerator.generateId() : name;
		this.description = description;
		this.inits = inits;
		this.childTasks = childTasks;
	}
	
	public boolean hasChildTasks() {
		return (childTasks != null) && !childTasks.isEmpty();
	}
	
	public TaskList getChildTasks() {
		return childTasks;
	}
	
	public void addExtension(TaskExtension taskExtension) {
		extensions.add(taskExtension);
	}
	
	public static String getInputParameterName(String tokenStr) throws InvalidInputException {
		ParameterReference parameterReference = ParameterReference.parse(tokenStr);
		return (parameterReference != null) && (parameterReference.isInputParameter()) ? parameterReference.getParameterPath() : "";
	}
	
	public abstract ITaskExecutor getTaskExecutor(ITaskExecutionStatusListener executionStatusListener);
}
