package com.livincloud.mastermind.core.domain;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * A collection of tasks that define the flow
 * 
 * @author hpmenon
 *
 */
public class Scenario {

	@Getter @Setter private String name;
	
	@Getter @Setter private String description;
	
	@Getter @Setter private TaskList taskList;
	
	@Getter private Map<String, ISearchable> inputParameters = new HashMap<String, ISearchable>();
	
	public void addInputParameter(String name, ISearchable value) {
		inputParameters.put(name, value);
	}
}