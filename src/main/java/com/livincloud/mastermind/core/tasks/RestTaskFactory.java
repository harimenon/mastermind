package com.livincloud.mastermind.core.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ITaskFactory;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.http.HttpRequestParams;
import com.livincloud.mastermind.http.IRestService;

@Component
public class RestTaskFactory implements ITaskFactory {
	
	@Autowired
	IRestService restService;
	
	@Override
	public Task createTask(ITaskProperties taskProperties) throws InvalidInputException {
		return new RestTask(
				taskProperties.extractTaskName(), 
				taskProperties.extractTaskDescription(),
				taskProperties.extractTaskInitializers(),
				restService, 
				buildHttpRequest(taskProperties));
	}

	private HttpRequestParams buildHttpRequest(ITaskProperties taskProperties) throws InvalidInputException {
		String api = taskProperties.extractString("GET");
		if(api != null) {
			String url = api;
			String queryParams = "";
	
			// Split into url, queryParams
			int indexOfQueryParams = api.indexOf("?");
			if( (indexOfQueryParams != -1) && ((indexOfQueryParams+1) < api.length()) ) {
				queryParams = api.substring(indexOfQueryParams + 1).trim();
				url = api.substring(0, indexOfQueryParams).trim();
			}
			
			return new HttpRequestParams(HttpMethod.GET, url, queryParams, taskProperties.extractString("body")); // TODO: headers
		}
		
		throw new InvalidInputException(taskProperties.toString(), "Only GET is supported at this time");
	}
}
