package com.livincloud.mastermind.core.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.ITaskOutput;
import com.livincloud.mastermind.util.JsonTree;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestTaskOutput implements ITaskOutput {
	public static final String RESPONSE_HEADERS = "headers.";
	
	private JsonTree jsonResponse;
	private Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
	
	private RestTaskOutput() {
	}
	
	@Override
	public List<ISearchable> search(String searchPath) throws InvalidInputException {
		List<Object> results = new ArrayList<Object>();
		
		if(searchPath.startsWith(RESPONSE_HEADERS)) {
			// Look in response headers
			searchPath = searchPath.substring(RESPONSE_HEADERS.length());
			log.trace("Searching in response headers: {}", searchPath);
			List<String> headerList = responseHeaders.get(searchPath.toLowerCase());
			if(headerList != null)
				results = new ArrayList<Object>(headerList);
		}
		else {
			// Look in results map
			log.trace("Searching in response map: {}", searchPath);
			results = jsonResponse.find(searchPath);
		}
		
		return ISearchable.SearchableJsonTreeNode.mapList(results);
	}
	
	public List<String> getResponseHeaders(String headerName) {
		return Collections.unmodifiableList(responseHeaders.get(headerName));
	}
	
	public static class Builder {
		private RestTaskOutput output = new RestTaskOutput();
				
		public Builder setJsonResponse(LinkedTreeMap<?,?> jsonResponse) {
			output.jsonResponse = new JsonTree(jsonResponse);
			return this;
		}
		
		public Builder addResponseHeaders(String name, List<String> values) {
			output.responseHeaders.put(name.toLowerCase(), values);
			return this;
		}
		
		public RestTaskOutput build() {
			return output;
		}
	}
}
