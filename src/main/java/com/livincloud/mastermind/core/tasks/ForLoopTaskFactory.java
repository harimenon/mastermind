package com.livincloud.mastermind.core.tasks;

import org.springframework.stereotype.Component;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ITaskFactory;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.util.Property;

@Component
public class ForLoopTaskFactory implements ITaskFactory {
	
	private static final String FOR_EACH = "foreach";
	private static final String FOR_EACH_PARALLEL = "foreach.parallel";

	@Override
	public Task createTask(ITaskProperties taskProperties) throws InvalidInputException, ServiceException {
		
		boolean parallelIteration = false;
		Property forLoopIterator = taskProperties.extractProperty(FOR_EACH, true);
		if(forLoopIterator == null) {
			forLoopIterator = taskProperties.extractProperty(FOR_EACH_PARALLEL, true);
			if(forLoopIterator == null)
				throw new InvalidInputException(taskProperties.toString(), "Missing iterator for " + FOR_EACH);
			parallelIteration = true;
		}
		else {
			if(taskProperties.extractProperty(FOR_EACH_PARALLEL, true) != null)
				throw new InvalidInputException(taskProperties.toString(), "Cannot specify more than one iterator. Remove " + FOR_EACH_PARALLEL);
		}
		
		return new ForLoopTask(
				taskProperties.extractTaskName(), 
				taskProperties.extractTaskDescription(),
				taskProperties.extractTaskInitializers(),
				forLoopIterator,
				parallelIteration,
				taskProperties.extractChildTasks());
	}
}
