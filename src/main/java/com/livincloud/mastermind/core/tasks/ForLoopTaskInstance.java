package com.livincloud.mastermind.core.tasks;

import java.util.List;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.Executor;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.core.execution.TaskInstance;
import com.livincloud.mastermind.core.execution.TaskListExecutor;
import com.livincloud.mastermind.util.Property;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ForLoopTaskInstance extends TaskInstance {
	private Property forEach;
	private boolean parallelIteration;
	
	public ForLoopTaskInstance(Task task, ExecutionContext ctx, Property forEach, boolean parallelIteration) {
		super(task, ctx);
		this.forEach = forEach;
		this.parallelIteration = parallelIteration;
	}

	@Override
	public boolean executeChildTasks(TaskListExecutor childTaskExecutor) throws TaskExecutionException {
		boolean continueOn = true;

		try {
			// Evaluate the criteria for the for loop
			List<ISearchable> results = taskReferenceResolver.findReferenceValues(forEach.getValue());
			
			// parallel execution or sequential execution of tasks?
			if(parallelIteration)
				log.debug("Parallel iteration enabled for iterator: {}", forEach.getKey());

			// Execute the tasks
			continueOn = Executor.execute(ctx, results, parallelIteration, new Executor.IExecutionCallback<ISearchable>() {
				@Override
				public boolean execute(ExecutionContext ctx, ISearchable result) {
					// Invoke the passed in child tasks executor. 
					// (Add the result to this context)
					return childTaskExecutor.execute(ctx.addInputParameter(forEach.getKey(), result));
				}
			});
		}
		catch(InvalidInputException e) {
			log.error("Error executing task [{}]: {}", getTaskName(), e.getMessage());
			throw new TaskExecutionException(e);
		}
		
		return continueOn && super.execute();
	}
}
