package com.livincloud.mastermind.core.tasks;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskList;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskExecutor;
import com.livincloud.mastermind.util.Property;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * For loop task
 * 
 * @author hpmenon
 *
 */
@EqualsAndHashCode(callSuper=true)
public class ForLoopTask extends Task {

	@Getter private Property forEach;
	@Getter private boolean parallelIteration;
	
	public ForLoopTask(String name, String description, Properties inits, Property forEach, boolean parallelIteration, TaskList tasks) {
		super(name, description, inits, tasks);
		this.forEach = forEach;
		this.parallelIteration = parallelIteration;
	}
	
	@Override
	public ITaskExecutor getTaskExecutor(ITaskExecutionStatusListener executionStatusListener) {
		return new ForLoopTaskExecutor(this, executionStatusListener);
	}
}
