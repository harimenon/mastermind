package com.livincloud.mastermind.core.tasks;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskExecutor;
import com.livincloud.mastermind.http.HttpRequestParams;
import com.livincloud.mastermind.http.IRestService;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Task to call a REST api
 * 
 * @author hpmenon
 *
 */
@EqualsAndHashCode(callSuper=true)
public class RestTask extends Task {

	@Getter private HttpRequestParams requestParams;
	
	@Getter private IRestService restService;
	
	public RestTask(String name, String description, Properties inits, IRestService iRestService, HttpRequestParams requestParams) {
		super(name, description, inits); // TODO: Should REST task support child tasks?
		this.restService = iRestService;
		this.requestParams = requestParams;
	}
	
	@Override
	public ITaskExecutor getTaskExecutor(ITaskExecutionStatusListener executionStatusCallback) {
		return new RestTaskExecutor(this, executionStatusCallback);
	}
}
