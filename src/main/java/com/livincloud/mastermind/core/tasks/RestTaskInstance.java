package com.livincloud.mastermind.core.tasks;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.core.execution.TaskInstance;
import com.livincloud.mastermind.http.HttpRequestParams;
import com.livincloud.mastermind.http.HttpServiceException;
import com.livincloud.mastermind.http.IRestService;
import com.livincloud.mastermind.util.JsonParser;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestTaskInstance extends TaskInstance {
	
	@Getter private HttpRequestParams httpParams;

	private IRestService restService;
	private JsonParser jsonParser = new JsonParser();
	
	public RestTaskInstance(Task restTask, ExecutionContext ctx, IRestService restService, HttpRequestParams httpParams) {
		super(restTask, ctx);
				
		this.restService = restService;
		this.httpParams = httpParams;
	}
	
	@Override
	public boolean execute() throws TaskExecutionException {
		boolean success = false;

		try {
			ResponseEntity<String> response = null;
			
			HttpRequestParams resolvedHttpParams = httpParams.resolveRuntimeReferences(this);
			
			String url = resolvedHttpParams.getFullUrl();
			log.debug("{}: {}", resolvedHttpParams.getRequestMethod(), url);
			
			Timer timer = startTimer();
			switch(httpParams.getRequestMethod()) {
				case GET:
					response = doGET(restService, url, resolvedHttpParams);
					success = true;
					break;
					
				case POST:
					response = doPOST(restService, url, resolvedHttpParams);
					success = true;
					break;
					
				default:
					break;
			}
			registerExecutionTime(timer.end());
			
			publishOutput(response);
		}
		catch(HttpServiceException httpEx) {
			log.error("Error executing task [{}]: {}", getTaskName(), httpEx.getMessage());
			throw new TaskExecutionException(httpEx.getMessage());
		}
		catch(InvalidInputException e) {
			log.error("Error executing task [{}]: {}", getTaskName(), e.getMessage());
			throw new TaskExecutionException(e);
		}
		
		return success && super.execute();
	}
	
	private void publishOutput(ResponseEntity<String> response) {
		RestTaskOutput.Builder builder = new RestTaskOutput.Builder();
		
		// The JSON response
		if( (response != null) && !response.getBody().isEmpty() ) {
			builder.setJsonResponse(jsonParser.parse(response.getBody()));
		}

		// HTTP response headers
		response.getHeaders().forEach((name,values) -> builder.addResponseHeaders(name, values));
		
		addTaskOutput(builder.build());
	}
	
	private static ResponseEntity<String> doGET(IRestService restService, String url, HttpRequestParams resolvedHttpParams) throws HttpServiceException, InvalidInputException {
		
		ResponseEntity<String> response = restService.get(url, resolvedHttpParams.getAuth(), resolvedHttpParams.getHeaders());
    	if((response == null) || (response.getStatusCode() != HttpStatus.OK))
    		throw new HttpServiceException(url, (response == null) ? null : response.getStatusCode());
		
		return response;
	}
	
	private static ResponseEntity<String> doPOST(IRestService restService, String url, HttpRequestParams resolvedHttpParams) throws HttpServiceException, InvalidInputException {
		
		ResponseEntity<String> response = restService.post(url, resolvedHttpParams.getBody(), resolvedHttpParams.getAuth(), resolvedHttpParams.getHeaders());
    	if((response == null) || (response.getStatusCode() != HttpStatus.OK))
    		throw new HttpServiceException(url, (response == null) ? null : response.getStatusCode());
		
		return response;
	}
}
