package com.livincloud.mastermind.core.tasks;

import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutor;
import com.livincloud.mastermind.http.HttpRequestParams;
import com.livincloud.mastermind.http.IRestService;

public class RestTaskExecutor extends TaskExecutor {
	private IRestService restService;
	private HttpRequestParams httpParams;

	public RestTaskExecutor(RestTask restTask, ITaskExecutionStatusListener executionStatusCallback) {
		super(restTask, executionStatusCallback);
		this.restService = restTask.getRestService();
		this.httpParams = restTask.getRequestParams().clone();
	}

	@Override
	protected ITaskInstance createExecutionInstance(ExecutionContext ctx) {
		return new RestTaskInstance(getTask(), ctx, restService, httpParams);
	}
}
