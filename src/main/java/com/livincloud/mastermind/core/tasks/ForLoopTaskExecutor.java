package com.livincloud.mastermind.core.tasks;

import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutor;
import com.livincloud.mastermind.util.Property;

import lombok.Getter;

public class ForLoopTaskExecutor extends TaskExecutor {
	private Property forEach;
	@Getter private boolean parallelIteration;
	
	public ForLoopTaskExecutor(ForLoopTask forLoopTask, ITaskExecutionStatusListener executionStatusListener) {
		super(forLoopTask, executionStatusListener);
		this.forEach = forLoopTask.getForEach();
		this.parallelIteration = forLoopTask.isParallelIteration();
	}

	@Override
	protected ITaskInstance createExecutionInstance(ExecutionContext ctx) {
		return new ForLoopTaskInstance(getTask(), ctx, forEach, parallelIteration);
	}
}
