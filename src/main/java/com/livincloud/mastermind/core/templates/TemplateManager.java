package com.livincloud.mastermind.core.templates;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.livincloud.mastermind.util.JsonParser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateManager {

	private Map<String, Template> templates = new HashMap<String, Template>();

	private JsonParser jsonParser = new JsonParser();

	public void addTemplate(String templatePrefix, Template template) {
		String fullName = templatePrefix + "." + template.getName();
		log.trace("Adding template [{}]: {}", fullName, template.toString());
		templates.put(fullName, template);
	}

	public Template getTemplate(String name) {
		return templates.get(name);
	}

	public <T extends TemplatedEntity> T createTemplatedEntity(String name, Properties paramValues, Class<T> className) {
		T templatedEntity = null;
		
		Template template = templates.get(name);
		if(template != null) {
			templatedEntity = jsonParser.parse(template.getValue(), className);
			if((templatedEntity != null) && (paramValues != null))
				templatedEntity.replaceTemplateParameters(paramValues);
		}
		
		return templatedEntity;
	}
}
