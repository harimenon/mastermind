package com.livincloud.mastermind.core.templates;

import com.google.gson.internal.LinkedTreeMap;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Template {
	private String type;
	private String name;
	private LinkedTreeMap<?,?> value;
}
