package com.livincloud.mastermind.core.templates;

import java.util.Properties;

import lombok.Data;

@Data
public abstract class TemplatedEntity {

	private String name;
	
	public void replaceTemplateParameters(Properties templateParameters) {
		for(Object name : templateParameters.keySet())
			resolveTemplateParams(name.toString(), templateParameters.getProperty(name.toString()));
	}

	protected abstract void resolveTemplateParams(String paramName, String paramValue);
}
