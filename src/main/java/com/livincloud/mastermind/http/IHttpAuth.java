package com.livincloud.mastermind.http;

import org.springframework.boot.web.client.RestTemplateBuilder;

public interface IHttpAuth {
	
	RestTemplateBuilder addAuth(RestTemplateBuilder restTemplateBuilder);
}
