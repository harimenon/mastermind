package com.livincloud.mastermind.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RestService implements IRestService {
	
	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	
	@Override
	public ResponseEntity<String> get(String url) 
			throws HttpServiceException {
		return get(url, null, null);
	}
	
	@Override
	public ResponseEntity<String> get(String url, IHttpAuth auth, HttpHeaderDefinitions headers) 
			throws HttpServiceException {
		return get(url, auth, headers, String.class);
	}

	@Override
	public <BODY> ResponseEntity<String> post(String url, BODY body) throws HttpServiceException {
		return post(url, body, null, null);
	}

	@Override
	public <BODY> ResponseEntity<String> post(String url, BODY body, IHttpAuth auth, HttpHeaderDefinitions headers) throws HttpServiceException {
		return post(url, body, auth, headers, String.class);
	}
	
	@Override
	public <RESPONSE> ResponseEntity<RESPONSE> get(String url, IHttpAuth auth, HttpHeaderDefinitions headerDefinitions, Class<RESPONSE> responseType) 
			throws HttpServiceException {
		
		try {
	    	// headers
	    	HttpHeaders headers = new HttpHeaders();
			if(headerDefinitions != null) {
		    	headerDefinitions.getGetHeaders().forEach((k,v) -> {
		    		log.trace("Adding HTTP Header: {}={}", k.toString(), v.toString());
		    		headers.set(k.toString(), v.toString());
		    	});
		        //headers.setContentType(MediaType.valueOf("application/vnd.oracle.adf.resourceitem+json"));
			}
	        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
			
	    	log.trace("GET: {}", url);
	    	ResponseEntity<RESPONSE> response = buildRestTemplate(auth).exchange(url, HttpMethod.GET, entity, responseType);
	    	
	    	log.trace("GET Response: {}", response.getStatusCode());
	    	return response;
		}
		catch(RestClientException e) {
			throw new HttpServiceException(url, e);
		}
	}

	@Override
	public <BODY,RESPONSE> ResponseEntity<RESPONSE> post(String url, BODY body, IHttpAuth auth, HttpHeaderDefinitions headerDefinitions, Class<RESPONSE> responseType) 
			throws HttpServiceException {
		
		try {
	    	// headers
	    	HttpHeaders headers = new HttpHeaders();
			if(headerDefinitions != null) {
		    	headerDefinitions.getPostHeaders().forEach((k,v) -> {
		    		log.trace("Adding HTTP Header: {}={}", k.toString(), v.toString());
		    		headers.set(k.toString(), v.toString());
		    	});
		        //headers.setContentType(MediaType.valueOf("application/vnd.oracle.adf.resourceitem+json"));
			}
			else {
		        headers.setContentType(MediaType.valueOf("application/json"));
			}
	        HttpEntity<BODY> entity = new HttpEntity<BODY>(body, headers);
			
	    	// TODO: Fix this correctly
	    	log.trace("POST: {}", url);
	    	ResponseEntity<RESPONSE> response = buildRestTemplate(auth).exchange(url, HttpMethod.POST, entity, responseType);
	    	
	    	log.trace("POST Response: {}", response.getStatusCode());
	    	return response;
		}
		catch(RestClientException e) {
			throw new HttpServiceException(url, e);
		}
	}
	
	private RestTemplate buildRestTemplate(IHttpAuth auth) {
		RestTemplateBuilder rtBuilder = (auth == null) ? restTemplateBuilder : auth.addAuth(restTemplateBuilder);
		return rtBuilder
				.setConnectTimeout(10000)
				.setReadTimeout(10000)
				.build();
	}
}
