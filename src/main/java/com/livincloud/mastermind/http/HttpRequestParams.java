package com.livincloud.mastermind.http;

import org.springframework.http.HttpMethod;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.execution.ITaskInstance;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public final class HttpRequestParams {

	private HttpMethod requestMethod;
	
	private IHttpAuth auth;
	
	private HttpHeaderDefinitions headers;
	
	private String url;
	
	private String queryParameters;
	
	private String body;
	
	private HttpRequestParams() {
	}
	
	public HttpRequestParams(HttpMethod requestMethod, String url, String queryParameters, String body) {
		this.requestMethod = requestMethod;
		this.url = url;
		this.queryParameters = queryParameters;
		this.body = body;
		this.auth = null;
		this.headers = null;
	}

	public HttpRequestParams(HttpMethod requestMethod, String url, String queryParameters, String body, IHttpAuth auth, HttpHeaderDefinitions headers) {
		this.requestMethod = requestMethod;
		this.url = url;
		this.queryParameters = queryParameters;
		this.body = body;
		this.auth = auth;
		this.headers = (headers == null) ? null : headers.clone();
	}
	
	public HttpRequestParams clone() {
		return new HttpRequestParams(
				getRequestMethod(),
				getUrl(), 
				getQueryParameters(),
				getBody(),
				getAuth(),
				getHeaders());
	}
	
	public void addQueryParameters(String params) {
		queryParameters += (queryParameters.isEmpty() ? params : "&" + params);
	}
	
	public HttpRequestParams resolveRuntimeReferences(ITaskInstance taskInstance) throws InvalidInputException {
		HttpRequestParams resolvedHttpParams = new HttpRequestParams();
		
		if(requestMethod != null)
			resolvedHttpParams.requestMethod = requestMethod;
		
		if(url != null)
			resolvedHttpParams.url = taskInstance.resolveRuntimeReferencesAsString(url);
		
		if(queryParameters != null)
			resolvedHttpParams.queryParameters = taskInstance.resolveRuntimeReferencesAsString(queryParameters);
		
		if(body != null)
			resolvedHttpParams.body = taskInstance.resolveRuntimeReferencesAsString(body);
		
		if(auth != null)
			resolvedHttpParams.auth = auth;
		
		if(headers != null) {
			resolvedHttpParams.headers = headers.resolveRuntimeReferences(taskInstance);
		}

		return resolvedHttpParams;
	}
	
	public String getFullUrl() {
		String fullUrl = url;
		if((queryParameters != null) && !queryParameters.isEmpty())
			fullUrl += "?" + queryParameters;
		return fullUrl;  
	}
}
