package com.livincloud.mastermind.http;

import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.execution.ITaskInstance;

import lombok.Getter;

@Getter
public class HttpHeaderDefinitions {

	Properties getHeaders = new Properties();
	
	Properties postHeaders = new Properties();
	
	private HttpHeaderDefinitions() {
	}
	
	public HttpHeaderDefinitions(Properties getHeaders, Properties postHeaders) {
		if(getHeaders != null)
			this.getHeaders = getHeaders;
		
		if(postHeaders != null)
			this.postHeaders = postHeaders;
	}
	
	public HttpHeaderDefinitions clone() {

		Properties newGetHeaders = new Properties();
		Properties newPostHeaders = new Properties();
		
		getHeaders.forEach((k,v) -> newGetHeaders.put(k.toString(), v.toString()));
		postHeaders.forEach((k,v) -> newPostHeaders.put(k.toString(), v.toString()));
		
		return new HttpHeaderDefinitions(newGetHeaders, newPostHeaders);
	}
	
	public HttpHeaderDefinitions resolveRuntimeReferences(ITaskInstance taskInstance) throws InvalidInputException {
		HttpHeaderDefinitions headerDefinitions = new HttpHeaderDefinitions();
		for(Object key : getHeaders.keySet()) {
			headerDefinitions.getHeaders.put(key, taskInstance.resolveRuntimeReferencesAsString(getHeaders.get(key).toString()));
		}
		
		for(Object key : postHeaders.keySet()) {
			headerDefinitions.postHeaders.put(key, taskInstance.resolveRuntimeReferencesAsString(postHeaders.get(key).toString()));
		}
		return headerDefinitions;
	}
}
