package com.livincloud.mastermind.http;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.livincloud.mastermind.MasterMindException;

import lombok.Getter;

public class HttpServiceException extends MasterMindException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter private String url;
	@Getter private HttpStatus httpStatus = null;
	@Getter private RestClientException clientException = null;
	
	public HttpServiceException(String url, RestClientException clientException) {
		super(clientException.getMessage() + " - " + url);
		
		this.url = url;
		this.clientException = clientException;
	}
	
	public HttpServiceException(String url, HttpStatus status) {
		super(((status != null) ? status.toString() : "") + " - " + url);
		
		this.url = url;
		this.httpStatus = status;
	}
}
