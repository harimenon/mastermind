package com.livincloud.mastermind.http;

import org.springframework.boot.web.client.RestTemplateBuilder;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HttpBasicAuth implements IHttpAuth {
	
	private String username;
	private String password;
	
	public IHttpAuth clone() {
		return new HttpBasicAuth(username, password);
	}
	
	public RestTemplateBuilder addAuth(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthorization(username, password);
	}
}
