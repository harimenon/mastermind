package com.livincloud.mastermind.http;

import org.springframework.http.ResponseEntity;

public interface IRestService {
	ResponseEntity<String> get(String url) throws HttpServiceException;
	ResponseEntity<String> get(String url, IHttpAuth auth, HttpHeaderDefinitions headers) throws HttpServiceException;
	<RESPONSE> ResponseEntity<RESPONSE> get(String url, IHttpAuth auth, HttpHeaderDefinitions headers, Class<RESPONSE> responseType) throws HttpServiceException;
	
	<BODY> ResponseEntity<String> post(String url, BODY body) throws HttpServiceException;
	<BODY> ResponseEntity<String> post(String url, BODY body, IHttpAuth auth, HttpHeaderDefinitions headers) throws HttpServiceException;
	<BODY,RESPONSE> ResponseEntity<RESPONSE> post(String url, BODY body, IHttpAuth auth, HttpHeaderDefinitions headerDefinitions, Class<RESPONSE> responseType) throws HttpServiceException;
}
