package com.livincloud.mastermind;

public class MasterMindException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8485219388349033714L;

	public MasterMindException(String message) {
		super(message);
	}
}
