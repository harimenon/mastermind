package com.livincloud.mastermind;

import lombok.Getter;

public class InvalidInputException extends MasterMindException {
	private static final long serialVersionUID = -4364764779752087024L;
	
	@Getter private String inputString;

	public InvalidInputException(String inputString, String message) {
		super(message);
		this.inputString = inputString;
	}
	
	public String getDisplayMessage() {
		return getMessage() + "-in-" + inputString;
	}
}
