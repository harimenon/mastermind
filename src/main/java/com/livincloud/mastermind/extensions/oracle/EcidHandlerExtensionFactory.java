package com.livincloud.mastermind.extensions.oracle;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.extensions.HttpHeaderPublisherExtension;
import com.livincloud.mastermind.extensions.IExtensionFactory;

public class EcidHandlerExtensionFactory implements IExtensionFactory {

	@Override
	public TaskExtension createExtension(String extensionName, Properties extensionParams, Task task) {
		return new HttpHeaderPublisherExtension(extensionName, "x-oracle-dms-ecid");
	}
}
