package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.templates.TemplateManager;
import com.livincloud.mastermind.extensions.TemplatedExtensionFactory;

public class LoopHandlerExtensionFactory extends TemplatedExtensionFactory {

	@Override
	public TaskExtension createExtensionFromTemplate(String templateName, Properties templateParams, TemplateManager templateManager, Task task) {
		LoopTemplateDTO loopDTO = templateManager.createTemplatedEntity(templateName, templateParams, LoopTemplateDTO.class);
		if(loopDTO == null) {
			return null;
		}
		loopDTO.setWhileCondition(loopDTO.getWhileCondition().replaceAll("this\\.output\\.", task.getName() + "\\.output\\."));
		return new LoopHandlerExtension(templateName, loopDTO);
	}
}
