package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.templates.TemplatedEntity;
import com.livincloud.mastermind.util.TemplateParameterReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
class LoopTemplateDTO extends TemplatedEntity {
	
	private Properties inits;
	private String queryParams;
	private String whileCondition;
	private Properties iterators;

	@Override
	protected void resolveTemplateParams(String paramName, String paramValue) {
		if(inits != null)
			TemplateParameterReference.resolveParameter(inits, paramName, paramValue);
		if(queryParams != null)
			queryParams = TemplateParameterReference.resolveParameter(queryParams, paramName, paramValue);
		if(whileCondition != null)
			whileCondition = TemplateParameterReference.resolveParameter(whileCondition, paramName, paramValue);
		if(iterators != null)
			TemplateParameterReference.resolveParameter(iterators, paramName, paramValue);
	}
}
