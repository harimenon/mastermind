package com.livincloud.mastermind.extensions.standard;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.core.tasks.RestTaskInstance;
import com.livincloud.mastermind.util.ExpressionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class LoopHandlerExtension extends TaskExtension {
	
	LoopTemplateDTO loopDTO;
	
	public LoopHandlerExtension(String extensionName, LoopTemplateDTO loopDTO) {
		super(extensionName);
		this.loopDTO = loopDTO;
	}

	@Override
	public boolean beforeTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		
		try {
			if( (taskInstance instanceof RestTaskInstance) && (loopDTO.getQueryParams() != null)) {
				RestTaskInstance restTei = (RestTaskInstance) taskInstance;
				
				log.trace("Adding query parameters for pagination: {}", loopDTO.getQueryParams());
				restTei.getHttpParams().addQueryParameters(loopDTO.getQueryParams());
			}
	
			log.trace("Evaluating pagination inits: {}", loopDTO.getInits());
			taskInstance.updateInputParameters(loopDTO.getInits());
		}
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
		
		return true;
	}

	@Override
	public boolean afterTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		
		try {
			ExpressionHandler expressionHandler = new ExpressionHandler();
			
			log.trace("Evaluating loop iterators: {}", loopDTO.getIterators());
			taskInstance.updateInputParameters(loopDTO.getIterators());
			
			log.trace("Loop start: {}", taskInstance.getTaskName());
			
			boolean success = true;
			while (success && expressionHandler.booleanValue(taskInstance.resolveRuntimeReferencesAsString(loopDTO.getWhileCondition()))) {
				log.trace("Pagination executing task: {}", taskInstance.getTaskName());
				if(success = taskInstance.execute()) {
					log.trace("Evaluating pagination iterators: {}", loopDTO.getIterators());
					taskInstance.updateInputParameters(loopDTO.getIterators());
				}			
			}
	
			log.trace("Loop end: {}", taskInstance.getTaskName());
		}
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
		
		return true;
	}
}
