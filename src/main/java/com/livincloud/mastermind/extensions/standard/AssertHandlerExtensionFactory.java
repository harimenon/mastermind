package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.extensions.IExtensionFactory;

public class AssertHandlerExtensionFactory implements IExtensionFactory {

	@Override
	public TaskExtension createExtension(String extensionName, Properties extensionParams, Task task) {
		return new AssertHandlerExtension(extensionName, extensionParams);
	}
}
