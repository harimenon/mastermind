package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.templates.TemplateManager;
import com.livincloud.mastermind.extensions.TemplatedExtensionFactory;

public class ServiceDefinitionTemplateHandlerFactory extends TemplatedExtensionFactory {

	@Override
	public TaskExtension createExtensionFromTemplate(String templateName, Properties templateParams, TemplateManager templateManager, Task task) {
		ServiceDefinitionTemplateDTO apiSetDTO = templateManager.createTemplatedEntity(templateName, templateParams, ServiceDefinitionTemplateDTO.class);
		if(apiSetDTO == null) {
			return null;
		}
		return new ServiceDefinitionTemplateHandler(templateName, apiSetDTO);
	}
}
