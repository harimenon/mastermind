package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;

class SleepHandlerExtension extends TaskExtension {
	private static final int MAX_INTERVAL_BETWEEN_TASKS = 60000;
	
	private int sleepDurationMillis;
	
	public SleepHandlerExtension(String extensionName, Properties props) {
		super(extensionName);
		sleepDurationMillis = Math.max(MAX_INTERVAL_BETWEEN_TASKS, Integer.parseInt(props.getProperty("durationMillis")));
	}

	@Override
	public boolean afterTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		try {
			Thread.sleep(sleepDurationMillis);
			return true;
		}
		catch (InterruptedException e) {
			throw new TaskExecutionException(e.getMessage());
		}
	}
}
