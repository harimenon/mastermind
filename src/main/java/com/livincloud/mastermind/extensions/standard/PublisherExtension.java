package com.livincloud.mastermind.extensions.standard;

import java.util.List;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PublisherExtension extends TaskExtension {
	
	private Properties valuesToPublish;
	
	public PublisherExtension(String extensionName, Properties toPublish) {
		super(extensionName);
		this.valuesToPublish = toPublish;
	}

	@Override
	public boolean withTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		try {
			log.trace("Publishing values: total={}", taskInstance.getTaskName(), valuesToPublish.size());
			for(Object toPublish : valuesToPublish.keySet()) {
				List<ISearchable> resolvedPublishTags = taskInstance.resolveRuntimeReferences(toPublish.toString());
				for(ISearchable resolverdTag : resolvedPublishTags) {
					ctx.addResults(
							taskInstance.getTaskName(),
							resolverdTag.toString(), 
							taskInstance.resolveRuntimeReferences(valuesToPublish.getProperty(toPublish.toString())));
				}
			}
			return true;
		}
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
	}
}
