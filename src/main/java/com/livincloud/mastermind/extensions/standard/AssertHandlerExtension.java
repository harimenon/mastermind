package com.livincloud.mastermind.extensions.standard;

import java.util.List;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.util.ExpressionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class AssertHandlerExtension extends TaskExtension {
	
	private Properties asserts;
	ExpressionHandler expressionHandler = new ExpressionHandler();
	
	public AssertHandlerExtension(String extensionName, Properties asserts) {
		super(extensionName);
		this.asserts = asserts;
	}

	@Override
	public boolean afterTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		try {
			boolean conditionTrue = true;
			for(Object _assertName : asserts.keySet()) {
				log.debug("Running assert: {}", _assertName);
				
				List<ISearchable> resolvedAsserts = taskInstance.resolveRuntimeReferences(asserts.getProperty(_assertName.toString()));
				for(ISearchable resolvedAssert : resolvedAsserts) {
					try {
						log.trace("Running resolved assert [{}]: {}", _assertName, resolvedAssert);
						boolean isAssertTrue = expressionHandler.booleanValue(resolvedAssert.toString());
						if(!isAssertTrue) {
							throw new TaskExecutionException(taskInstance.getTaskName(), "Assert failed: " + resolvedAssert);
						}
					}
					catch(TaskExecutionException teEx) {
						throw teEx;
					}
					catch(Exception spelEx) {
						log.error("Invalid assert: {}", spelEx.getMessage());
						throw new TaskExecutionException(taskInstance.getTaskName(), "Invalid assert: " + resolvedAssert + " - " + spelEx.getMessage());
					}
				}

			}
			return conditionTrue;
		}
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
	}
}
