package com.livincloud.mastermind.extensions.standard;

import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.core.tasks.RestTaskInstance;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class ServiceDefinitionTemplateHandler extends TaskExtension {
	
	private ServiceDefinitionTemplateDTO serviceDefinitionTemplateDTO;
	
	public ServiceDefinitionTemplateHandler(String extensionName, ServiceDefinitionTemplateDTO serviceDefinitionTemplateDTO) {
		super(extensionName);
		this.serviceDefinitionTemplateDTO = serviceDefinitionTemplateDTO;
	}

	@Override
	public boolean beforeTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		
		if(taskInstance instanceof RestTaskInstance) {
			RestTaskInstance restTaskInstance = (RestTaskInstance) taskInstance;
			
			log.trace("Updating the HTTP parameters");
			
			restTaskInstance.getHttpParams().setUrl(serviceDefinitionTemplateDTO.buildFullUrl(restTaskInstance.getHttpParams().getUrl()));
			restTaskInstance.getHttpParams().setAuth(serviceDefinitionTemplateDTO.getBasicAuth().toHttpBasicAuth());
			restTaskInstance.getHttpParams().setHeaders(serviceDefinitionTemplateDTO.getHeaders().toHttpHeaderDefinitions());
		}
		
		return true;
	}
}
