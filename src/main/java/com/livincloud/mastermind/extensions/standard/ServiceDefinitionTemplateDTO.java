package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.core.templates.TemplatedEntity;
import com.livincloud.mastermind.http.HttpBasicAuth;
import com.livincloud.mastermind.http.HttpHeaderDefinitions;
import com.livincloud.mastermind.util.TemplateParameterReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
class ServiceDefinitionTemplateDTO extends TemplatedEntity {

	private String baseUrl;
	private HttpBasicAuthDTO basicAuth;
	private HttpHeaderDefinitionsDTO headers;
	
	public String buildFullUrl(String apiPath) {
		String url = baseUrl;
		int apiStart = apiPath.indexOf(":");
		if(!apiPath.startsWith("/") && (apiStart != -1))
			url += apiPath.substring(apiStart + 1);
		else
			url += apiPath;
		return url;
	}
	
	@Override
	protected void resolveTemplateParams(String paramName, String paramValue) {
		if(baseUrl != null)
			baseUrl = TemplateParameterReference.resolveParameter(baseUrl, paramName, paramValue);
		if(basicAuth != null) {
			if(basicAuth.getUsername() != null)
				basicAuth.setUsername(TemplateParameterReference.resolveParameter(basicAuth.getUsername(), paramName, paramValue));
			if(basicAuth.getPassword() != null)
				basicAuth.setPassword(TemplateParameterReference.resolveParameter(basicAuth.getPassword(), paramName, paramValue));
		}
		// headers
	}
	
	@Data
	class HttpHeaderDefinitionsDTO {
		Properties GET;
		Properties POST;
		
		public HttpHeaderDefinitions toHttpHeaderDefinitions() {
			return new HttpHeaderDefinitions(GET, POST);
		}
	}

	@Data
	class HttpBasicAuthDTO {
		private String username;
		private String password;
		
		public HttpBasicAuth toHttpBasicAuth() {
			return new HttpBasicAuth(username, password);
		}
	}
}