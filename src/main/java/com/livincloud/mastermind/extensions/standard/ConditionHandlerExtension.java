package com.livincloud.mastermind.extensions.standard;

import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.util.ExpressionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class ConditionHandlerExtension extends TaskExtension {
	
	private Properties conditions;
	
	public ConditionHandlerExtension(String extensionName, Properties conditions) {
		super(extensionName);
		this.conditions = conditions;
	}

	@Override
	public boolean beforeTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		try {
			boolean conditionTrue = true;
			for(Object condition : conditions.keySet()) {
				log.trace("Evaluating condition: {}", condition);
				if(condition.toString().equalsIgnoreCase("IF")) {
					conditionTrue = conditionTrue && (new ExpressionHandler()).booleanValue(taskInstance.resolveRuntimeReferencesAsString(conditions.getProperty(condition.toString())));
					if(!conditionTrue)
						break;
				}
			}
			return conditionTrue;
		}
		catch (InvalidInputException e) {
			throw new TaskExecutionException(e);
		}
	}
}
