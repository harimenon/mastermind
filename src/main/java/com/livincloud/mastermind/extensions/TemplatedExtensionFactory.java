package com.livincloud.mastermind.extensions;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;

public abstract class TemplatedExtensionFactory implements ITemplatedExtensionFactory {

	@Override
	public TaskExtension createExtension(String extensionName, Properties extensionParams, Task task) {
		return null;
	}
}
