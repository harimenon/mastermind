package com.livincloud.mastermind.extensions;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.templates.TemplateManager;

public interface ITemplatedExtensionFactory extends IExtensionFactory {
	TaskExtension createExtensionFromTemplate(String templateName, Properties templateParams, TemplateManager templateManager, Task task);
}
