package com.livincloud.mastermind.extensions;

import java.util.Properties;

import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;

public interface IExtensionFactory {
	TaskExtension createExtension(String extensionName, Properties extensionParams, Task task);
}
