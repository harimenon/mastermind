package com.livincloud.mastermind.extensions;

import java.util.List;

import com.livincloud.mastermind.core.domain.ITaskOutput;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.core.tasks.RestTaskOutput;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpHeaderPublisherExtension extends TaskExtension {
	
	private String headerName;
	
	public HttpHeaderPublisherExtension(String extensionName, String headerName) {
		super(extensionName);
		this.headerName = headerName;
	}

	@Override
	public boolean beforeTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		return true;
	}

	@Override
	public boolean afterTaskExecution(ITaskInstance taskInstance, ExecutionContext ctx) throws TaskExecutionException {
		ITaskOutput taskOutput = ctx.getTaskOutput(taskInstance.getTaskName());
		if(taskOutput instanceof RestTaskOutput) {
			RestTaskOutput restTaskOutput = (RestTaskOutput) taskOutput;
			List<String> ecids = restTaskOutput.getResponseHeaders(headerName);
			if(ecids != null) {
				log.trace("ECID: {}", ecids.get(0));
				ctx.addResults(taskInstance.getTaskName(), "ecid", ecids.get(0));
			}
		}
		return true;
	}
}
