package com.livincloud.mastermind.server;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.livincloud.mastermind.IErrorCode;

/**
 * All errors from the Server
 * 
 * @author hari
 *
 */
public enum ServerErrors implements IErrorCode {

    // REST
    INTERNAL_ERROR(1000, "Internal error"),
    INVALID_CONFIGURATION(1001, "Invalid server configuration: {0}"),
    INVALID_INPUT(1002, "Invalid input: {0} -in- {1}")
    ;

    // END OF ERRORS
    ////////////////////////////////////////////////////////////

	private final int code;
	private String description;
	private List<Object> attributes = new ArrayList<Object>();

	private ServerErrors(int code, String description) {
		this.code = code;
		this.description = description;
		this.attributes = new ArrayList<Object>();
	}

	public String getDescription() {
		return MessageFormat.format(description, attributes.toArray());
	}

	public ServerErrors addAttribute(Object attribute) {
		attributes.add(attribute);
		return this;
	}

	public ServerErrors addAttributes(Object[] attribs) {
		for (int i = 0; i < attribs.length; i++) {
			attributes.add(attribs[i]);
		}
		return this;
	}

	public int getCode() {
		return code;
	}
	
	public String toString() {
	    return getDescription();
	}

	public void setAttributes() {
		attributes = new ArrayList<Object>();
	}
}
