package com.livincloud.mastermind.server.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExecutionRecord {
	private String executionId;
	
	private String flowId;

	private String scenarioName;
	
	private String scenarioJson;
	
	private int threadCount;
	
	private String statusCallbackUrl;
}
