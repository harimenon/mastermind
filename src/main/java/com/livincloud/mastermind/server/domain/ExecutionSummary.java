package com.livincloud.mastermind.server.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ExecutionSummary {
	private String executionRequestId;
	private List<ScenarioExecutionStatus> scenarioStatusList = new ArrayList<ScenarioExecutionStatus>();
	
	public ExecutionSummary(String executionRequestId) {
		this.executionRequestId = executionRequestId;
	}

	public void addScenarioStatus(ScenarioExecutionStatus status) {
		scenarioStatusList.add(status);
	}

	public ScenarioExecutionStatus getScenarioStatus(String executionContextId) {
		for(ScenarioExecutionStatus status : scenarioStatusList) {
			if(status.getExecutionContextId().compareTo(executionContextId) == 0)
				return status;
		}
		return null;
	}
}
