package com.livincloud.mastermind.server.domain;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskExecutionStatus {
	public enum State { EXECUTING, COMPLETED, FAILED }
	
	private String executionRequestId;
	private String executionContextId;
	private String taskName;
	private State taskStatus;
	private Map<String,Object> results;
}
