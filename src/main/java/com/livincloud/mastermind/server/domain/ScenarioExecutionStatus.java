package com.livincloud.mastermind.server.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ScenarioExecutionStatus {
	public enum State { EXECUTING, COMPLETED, FAILED }
	
	private String executionContextId;
	private State status;
}
