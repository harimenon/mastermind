package com.livincloud.mastermind.server;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import com.livincloud.mastermind.util.LogUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EntityScan("com.livincloud.mastermind")
@ComponentScan({"com.livincloud.mastermind"})
@ImportResource("classpath:app-config.xml")
public class Application {
	
	@Autowired
	private TaskRegistry taskRegistry;
    
	@Autowired
    ExtensionRegistry extensionRegistry;
    
	public Application() {
	}
	
	@EventListener
	public void serverInit(ContextRefreshedEvent event) {
		
		LogUtils.setContext("OFLOW");
		
		try {
			ClassLoader classLoader = new ClassLoader(event.getApplicationContext());
			taskRegistry.registerTasks(classLoader);
			extensionRegistry.registerExtensions(classLoader);
		}
		catch(ServiceException e) {
			log.error("Server failed to start up: " + e.getMessage());
		}
		finally {
			LogUtils.clearContext();
		}
	}

	public static void main(String[] args) {
    	SpringApplication.run(Application.class, args);
    }
	
	static class ClassLoader {
		ApplicationContext applicationContext;
		
		ClassLoader(ApplicationContext applicationContext) {
			this.applicationContext = applicationContext;
		}
		
		public <T> T loadClass(String className, Class<T> type) throws ServiceException {
			try {
				Class<?> classToLoad = this.getClass().getClassLoader().loadClass(className);
				
				// Try loading as a bean
				T instance = loadBean(classToLoad, type);
				if(instance != null)
					return instance;

				// Otherwise try as a class
				instance = loadClass(classToLoad, type);
				if(instance != null)
					return instance;
				
				throw new ServiceException(ServerErrors.INVALID_CONFIGURATION, className + " is not of type " + type.getName());
			}
			catch (Exception e) {
				String error = e.getClass() + ": " + e.getMessage();
				log.error("Error loading class [{}]: {}", className, error);
				throw new ServiceException(ServerErrors.INVALID_CONFIGURATION, className + " - "+ error);
			}
		}
		
		@SuppressWarnings("unchecked")
		private <T> T loadBean(Class<?> beanClass, Class<T> type) {
			try {
				log.trace("Loading bean: {}", beanClass.getName());
				T bean = (T) applicationContext.getBean(beanClass);
				return type.isInstance(bean) ? bean : null;
			}
			catch(NoSuchBeanDefinitionException e) {
			}
			return null;
		}
		
		@SuppressWarnings("unchecked")
		private <T> T loadClass(Class<?> classToLoad, Class<T> type) throws Exception {
			log.trace("Loading class: {}", classToLoad.getName());
			return type.isAssignableFrom(classToLoad) ? (T) classToLoad.getConstructor().newInstance() : null;
		}
	}
}
