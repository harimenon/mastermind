package com.livincloud.mastermind.server.persistence.service;

import java.util.Collection;

import com.livincloud.mastermind.server.domain.ExecutionRecord;

public interface IExecutionRecordStore {

	public ExecutionRecord saveExecutionRecord(ExecutionRecord executionRecord);

	public ExecutionRecord getExecutionRecord(String executionId);
	
	public Collection<ExecutionRecord> getAllExecutionRecords();
}
