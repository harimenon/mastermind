package com.livincloud.mastermind.server.persistence.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.livincloud.mastermind.server.domain.ExecutionRecord;

@Service
public class MemExecutonRecordStore implements IExecutionRecordStore {
	private Map<String, ExecutionRecord> executionRecords = new HashMap<String, ExecutionRecord>();
	
	@Override
	public synchronized ExecutionRecord saveExecutionRecord(ExecutionRecord executionRecord) {
		
		executionRecords.put(executionRecord.getExecutionId(), executionRecord);
		
		return executionRecord;
	}

	@Override
	public ExecutionRecord getExecutionRecord(String executionId) {
		return executionRecords.get(executionId);
	}

	@Override
	public Collection<ExecutionRecord> getAllExecutionRecords() {
		return executionRecords.values();
	}
}
