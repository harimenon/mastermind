package com.livincloud.mastermind.server.persistence.service;

import java.util.Collection;
import java.util.Map;

import com.livincloud.mastermind.server.domain.ExecutionSummary;
import com.livincloud.mastermind.server.domain.ScenarioExecutionStatus;
import com.livincloud.mastermind.server.domain.TaskExecutionStatus;

public interface IExecutionStatusStore {
	ExecutionSummary getExecutionStatus(String executionRequestId);
	Collection<ExecutionSummary> getExecutionStatusList();
	
	ExecutionSummary saveExecutionStatus(String executionRequestId, ScenarioExecutionStatus scenarioExecutionStatus);
	
	TaskExecutionStatus saveTaskStatus(String executionRequestId, TaskExecutionStatus taskExecutionStatus);
	TaskExecutionStatus getTaskStatus(String executionRequestId, String taskName);
	Map<String, Map<String, TaskExecutionStatus>> getTaskStatusList(String executionRequestId);
}
