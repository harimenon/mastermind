package com.livincloud.mastermind.server.persistence.service;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.livincloud.mastermind.server.domain.ExecutionSummary;
import com.livincloud.mastermind.server.domain.ScenarioExecutionStatus;
import com.livincloud.mastermind.server.domain.TaskExecutionStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MemExecutionStatusStore implements IExecutionStatusStore {

	private static Map<String, ExecutionSummary> executionStatusMap = new ConcurrentHashMap<String, ExecutionSummary>();
	private static Map<String, Map<String, Map<String, TaskExecutionStatus>>> taskStatusMap = new ConcurrentHashMap<String, Map<String, Map<String, TaskExecutionStatus>>>();
	
	@Override
	public ExecutionSummary getExecutionStatus(String executionRequestId) {
		return executionStatusMap.get(executionRequestId);
	}

	@Override
	public Collection<ExecutionSummary> getExecutionStatusList() {
		return executionStatusMap.values();
	}

	@Override
	public synchronized ExecutionSummary saveExecutionStatus(String executionRequestId, ScenarioExecutionStatus scenarioExecutionStatus) {
		ExecutionSummary executionSummary = executionStatusMap.get(executionRequestId);
		if(executionSummary == null) {
			executionSummary = new ExecutionSummary(executionRequestId);
			executionStatusMap.put(executionRequestId, executionSummary);
		}
		ScenarioExecutionStatus storedScenarioExecutionStatus = executionSummary.getScenarioStatus(scenarioExecutionStatus.getExecutionContextId());
		if(storedScenarioExecutionStatus == null)
			executionSummary.addScenarioStatus(scenarioExecutionStatus);
		else
			storedScenarioExecutionStatus.setStatus(scenarioExecutionStatus.getStatus());
		log.trace("Execution status saved: {}", executionSummary.toString());
		return executionSummary;
	}

	@Override
	public synchronized TaskExecutionStatus saveTaskStatus(String executionRequestId, TaskExecutionStatus taskExecutionStatus) {
		Map<String, Map<String, TaskExecutionStatus>> execContextMap = taskStatusMap.get(executionRequestId);
		if(execContextMap == null) {
			execContextMap = new ConcurrentHashMap<String, Map<String, TaskExecutionStatus>>();
			taskStatusMap.put(executionRequestId, execContextMap);
		}
		
		Map<String, TaskExecutionStatus> taskMap = execContextMap.get(taskExecutionStatus.getExecutionContextId());
		if(taskMap == null) {
			taskMap = new ConcurrentHashMap<String, TaskExecutionStatus>();
			execContextMap.put(taskExecutionStatus.getExecutionContextId(), taskMap);
		}
		taskMap.put(taskExecutionStatus.getTaskName(), taskExecutionStatus);
		log.trace("Task status saved: {}", taskStatusMap.toString());
		return taskExecutionStatus;
	}

	@Override
	public TaskExecutionStatus getTaskStatus(String executionRequestId, String taskName) {
		Map<String, Map<String, TaskExecutionStatus>> execContextMap = taskStatusMap.get(executionRequestId);
		if(execContextMap == null)
			return null;
		
		Map<String, TaskExecutionStatus> taskMap = execContextMap.get(executionRequestId);
		return (taskMap != null) ? taskMap.get(taskName) : null;
	}

	@Override
	public Map<String, Map<String, TaskExecutionStatus>> getTaskStatusList(String executionRequestId) {
		return taskStatusMap.get(executionRequestId);
	}
}
