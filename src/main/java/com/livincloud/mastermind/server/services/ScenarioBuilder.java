package com.livincloud.mastermind.server.services;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ISearchable;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskList;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.ServerErrors;
import com.livincloud.mastermind.server.rest.resources.ScenarioDTO;
import com.livincloud.mastermind.util.JsonParser;
import com.livincloud.mastermind.util.LogUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScenarioBuilder {
	
	private Scenario scenario;
	private TaskBuilder taskBuilder;
	
	/**
	 * Public constructor
	 * 
	 * @param templateManagerService
	 */
	public ScenarioBuilder(TaskBuilder taskBuilder) {
		this.taskBuilder = taskBuilder;
	}
	
	/**
	 * Build the Scenario object from the DTO
	 * 
	 * @param scenarioDTO
	 * @return the Scenario object
	 * @throws ServiceException
	 */
	public Scenario build(ScenarioDTO scenarioDTO) throws ServiceException {
		
		log.debug("Building Scenario: {}", scenarioDTO.getName());
		
		try {
			scenario = new Scenario();
			scenario.setName(scenarioDTO.getName());
			scenario.setDescription(scenarioDTO.getDescription());
			
			// Input parameters
			if(scenarioDTO.getInput() != null) {
				scenarioDTO.getInput().forEach((k,v) -> {scenario.addInputParameter(k, ISearchable.SearchableJsonTreeNode.map(v));} );
				log.debug("Input parameters: {}", scenario.getInputParameters().toString());
			}
			
			// The tasks - all tasks are serial for now.
			if(scenarioDTO.getTasks() == null) {
				throw new InvalidInputException("Scenario [" + scenarioDTO.getName() + "]", "No tasks defined in scenario!");
			}
			
			// ***
			// Create the tasks
			// ***
			TaskList taskList = new TaskList(scenarioDTO.isExecuteTasksInParallel());
			LogUtils.logHeader(log, "Building tasks: " + scenario.getName());
			JsonParser jsonParser = new JsonParser();
			for(LinkedTreeMap<?,?> taskPropertiesTree : scenarioDTO.getTasks()) {
				// Re-parse the task properties using the gson parser
				ITaskProperties taskProperties = new JsonTaskProperties(jsonParser.duplicate(taskPropertiesTree), taskBuilder);
				Task task = taskBuilder.buildTask(taskProperties);
				taskList.addTask(task);
			}
			scenario.setTaskList(taskList);
		} 
		catch (InvalidInputException e) {
			log.error("Invalid input: ", e.getMessage());
			throw new ServiceException(ServerErrors.INVALID_INPUT, e.getMessage(), e.getInputString());
		}
		
		return scenario;
	}
}
