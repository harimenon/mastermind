package com.livincloud.mastermind.server.services;

import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskExtension;
import com.livincloud.mastermind.core.templates.Template;
import com.livincloud.mastermind.core.templates.TemplateManager;
import com.livincloud.mastermind.extensions.IExtensionFactory;
import com.livincloud.mastermind.extensions.ITemplatedExtensionFactory;
import com.livincloud.mastermind.server.ExtensionRegistry;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.TaskRegistry;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskBuilder {
	private ExtensionRegistry extensionRegistry;
	private TemplateManager templateManager;
	private TaskRegistry taskRegistry;
	private Map<String, Properties> globalExtensions;
	
	public TaskBuilder(TaskRegistry taskRegistry, ExtensionRegistry extensionRegistry, TemplateManager templateManager, Map<String, Properties> globalExtensions) {
		this.taskRegistry = taskRegistry;
		this.extensionRegistry = extensionRegistry;
		this.templateManager = templateManager;
		this.globalExtensions = globalExtensions;
	}
	
	public Task buildTask(ITaskProperties taskProperties) throws ServiceException, InvalidInputException {
		// Create the task
		log.trace("Creating task: {}", taskProperties.toString());
		Task task = taskRegistry.createTask(taskProperties);
		log.debug("Task created [{}]: {}", task.getName(), task.getClass());
		
		// Add global extensions
		if(globalExtensions != null) {
			log.trace("Adding global extensions: {}", globalExtensions.size());
			for(String extensionName : globalExtensions.keySet()) {
				addExtension(task, extensionName, globalExtensions.get(extensionName));
			}
		}
		
		// Add task specific extensions
		for(Object extension : taskProperties.getPropertyNames()) {
			log.trace("Adding task extensions: {}", taskProperties.size());
			addExtension(task, extension.toString(), taskProperties.getProperties(extension.toString(), false));
		}
		
		return task;
	}
	
	private void addExtension(Task task, String extensionName, Properties extensionParams) throws InvalidInputException {
		log.trace("Adding extension [{}]: {} - properties={}", task.getName(), extensionName, extensionParams.toString());

		// Check if the extension is registered
		IExtensionFactory factory = extensionRegistry.getExtensionFactory(extensionName);
		if(factory != null) {
			TaskExtension extension = factory.createExtension(extensionName, extensionParams, task);
			addUniqueExtension(task, extension);
			log.trace("Extension added [{}]: {}", task.getName(), extension.getName());
			return;
		}
		// Check if a template is defined
		else if(templateManager.getTemplate(extensionName) != null) {
			Template template = templateManager.getTemplate(extensionName);
			IExtensionFactory templatedFactory = extensionRegistry.getExtensionFactory(template.getType());
			if((templatedFactory != null) && (templatedFactory instanceof ITemplatedExtensionFactory)) {
				TaskExtension extension = ((ITemplatedExtensionFactory)templatedFactory).createExtensionFromTemplate(extensionName, extensionParams, templateManager, task);
				addUniqueExtension(task, extension);
				log.trace("Extension added [{}]: {} - template used: {}", task.getName(), extension.getName(), template.getType());
				return;
			}
		}
		
		throw new InvalidInputException(extensionName, "Extension not found: " + extensionName);
	}
	
	private void addUniqueExtension(Task task, TaskExtension extension) {
		Optional<TaskExtension> existingExtension = task.getExtensions().stream().filter(e -> e.getName().equalsIgnoreCase(extension.getName())).findFirst();
		if(existingExtension.isPresent()) {
			log.trace("Overriding global extension: " + extension.getName());
			task.getExtensions().remove(existingExtension.get());
		}
		task.addExtension(extension);
	}
}
