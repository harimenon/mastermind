package com.livincloud.mastermind.server.services;

import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.domain.ExecutionRecord;
import com.livincloud.mastermind.server.rest.resources.ExecutionRequestDTO;

public interface IExecutionRequestProcessor {
	ExecutionRecord run(ExecutionRequestDTO executionRequestDTO) throws ServiceException;
}
