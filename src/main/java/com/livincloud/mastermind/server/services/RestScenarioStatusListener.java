package com.livincloud.mastermind.server.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.livincloud.mastermind.core.execution.ExecutionContext;
import com.livincloud.mastermind.core.execution.IScenarioExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskExecutionStatusListener;
import com.livincloud.mastermind.core.execution.ITaskInstance;
import com.livincloud.mastermind.core.execution.TaskExecutionException;
import com.livincloud.mastermind.http.HttpServiceException;
import com.livincloud.mastermind.http.IRestService;
import com.livincloud.mastermind.server.domain.ScenarioExecutionStatus;
import com.livincloud.mastermind.server.domain.TaskExecutionStatus;
import com.livincloud.mastermind.server.rest.resources.ExecutionRequestDTO;
import com.livincloud.mastermind.server.rest.resources.ScenarioExecutionStatusDTO;
import com.livincloud.mastermind.server.rest.resources.TaskExecutionStatusDTO;
import com.livincloud.mastermind.server.services.ExecutionRequestProcessor.ScenarioExecutionTracker;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestScenarioStatusListener implements IScenarioExecutionStatusListener,  ITaskExecutionStatusListener {

	private String executionRequestId;
	private ExecutionRequestDTO executionRequestDTO;
	private ScenarioExecutionTracker scenarioExecutionTracker;
	

	private IRestService restService;
	
	public RestScenarioStatusListener(IRestService restService, String executionRequestId, ExecutionRequestDTO executionRequestDTO, ScenarioExecutionTracker scenarioExecutionTracker) {
		this.restService = restService;
		this.executionRequestId = executionRequestId;
		this.executionRequestDTO = executionRequestDTO;
		this.scenarioExecutionTracker = scenarioExecutionTracker;
	}

	@Override
	public void onExecutionStart(ExecutionContext ctx) {
		log.info("Scenario execution STARTED");
		notifyScenarioExecutionStatus(ctx, ScenarioExecutionStatus.State.EXECUTING);
		scenarioExecutionTracker.notifyScenarioStart();
	}

	@Override
	public void onExecutionEnd(ExecutionContext ctx, boolean success) {
		notifyScenarioExecutionStatus(ctx, success ? ScenarioExecutionStatus.State.COMPLETED : ScenarioExecutionStatus.State.FAILED);
		log.info("Scenario execution {}", success ? "SUCCESS" : "FAILED");

		scenarioExecutionTracker.notifyScenarioEnd();
	}

	@Override
	public void onExecutionStart(ExecutionContext ctx, ITaskInstance taskInstance) {
		notifyTaskExecutionStatus(
				new TaskExecutionStatusDTO.Builder(executionRequestId, ctx.getId(), taskInstance.getTaskName())
				.setState(TaskExecutionStatus.State.EXECUTING)
				.build());
	}

	@Override
	public void onExecutionSuccess(ExecutionContext ctx, ITaskInstance taskInstance) {
		notifyTaskExecutionStatus(
				new TaskExecutionStatusDTO.Builder(executionRequestId, ctx.getId(), taskInstance.getTaskName())
				.setState(TaskExecutionStatus.State.COMPLETED)
				.addResults(ctx.getResultsMap(taskInstance.getTaskName()))
				.build());
	}

	@Override
	public void onExecutionFailure(ExecutionContext ctx, ITaskInstance taskInstance, TaskExecutionException taskExecutionException) {
		notifyTaskExecutionStatus(
				new TaskExecutionStatusDTO.Builder(executionRequestId, ctx.getId(), taskInstance.getTaskName())
				.setState(TaskExecutionStatus.State.FAILED)
				.addResult("exception", taskExecutionException)
				.build());
	}

	/*
	 * Notify scenario execution status
	 */
	private void notifyScenarioExecutionStatus(ExecutionContext ctx, ScenarioExecutionStatus.State state) {
		String callbackUrl = getScenarioExecutionStatusUrl(executionRequestId);
		
		if(callbackUrl == null)
			return;
		
        log.trace("Notifying execution status to: {}", callbackUrl);
        
		try {
	        // post results
	        ResponseEntity<String> response = restService.post(
	        		callbackUrl, 
	        		new ScenarioExecutionStatusDTO(ctx.getId(), state));
	        if(response.getStatusCode() != HttpStatus.OK) {
	        	log.error("Failed to send scenario status to {} - {}", callbackUrl, response.getStatusCode());
	        }
		}
		catch (HttpServiceException e) {
			log.error("Failed to send scenario status to {} - {}", callbackUrl, e.getMessage());
		}
	}

	/*
	 * Notify task execution status
	 */
	private void notifyTaskExecutionStatus(TaskExecutionStatusDTO taskExecutionStatusDTO) {
		String callbackUrl = getTaskExecutionStatusUrl(executionRequestId);
		
		if(callbackUrl == null)
			return;
				
		log.trace("Notifying task status to: {}", callbackUrl);
        
		try {
	        // post task status
	        ResponseEntity<String> response = restService.post(
	        		callbackUrl, 
	        		taskExecutionStatusDTO);
	        if(response.getStatusCode() != HttpStatus.OK) {
	        	log.error("Failed to send task status to {} - HTTP status: {}", callbackUrl, response.getStatusCode());
	        }
		}
		catch (HttpServiceException e) {
			log.error("Failed to send task status to {} - {}", callbackUrl, e.getMessage());
		}
	}
	
	private String getExecutionSummaryUrl() {
		if( (executionRequestDTO.getExecutionStatusUrl() == null) || (executionRequestDTO.getExecutionStatusUrl().isEmpty()) )
			return null;
		
		return executionRequestDTO.getExecutionStatusUrl() + "/results";
	}
	
	private String getExecutionSummaryUrl(String executionId) {
		String executionSummaryUrl = getExecutionSummaryUrl();
		if(executionSummaryUrl == null)
			return null;
		
		return executionSummaryUrl + "/" + executionId;
	}
	
	private String getScenarioExecutionStatusUrl(String executionId) {
		String executionSummaryUrl = getExecutionSummaryUrl(executionId);
		if(executionSummaryUrl == null)
			return null;
		
		return executionSummaryUrl + "/scenarios";
	}
	
	private String getTaskExecutionStatusUrl(String executionId) {
		String executionSummaryUrl = getExecutionSummaryUrl(executionId);
		if(executionSummaryUrl == null)
			return null;
		
		return executionSummaryUrl + "/tasks";
	}
}
