package com.livincloud.mastermind.server.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Task;
import com.livincloud.mastermind.core.domain.TaskList;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.util.JsonTree;
import com.livincloud.mastermind.util.Property;

import lombok.extern.slf4j.Slf4j;

/**
 * Task properties
 * 
 * @author hpmenon
 *
 */
@Slf4j
public class JsonTaskProperties extends JsonTree implements ITaskProperties {
	TaskBuilder taskBuilder;
	
	public JsonTaskProperties(LinkedTreeMap<?,?> taskPropertiesMap, TaskBuilder taskBuilder) {
		super(taskPropertiesMap);
		log.trace("Task parameters: {}", taskPropertiesMap);
		this.taskBuilder = taskBuilder;
	}
	
	@Override
	public Set<?> getPropertyNames() {
		return keySet();
	}
	
	@Override
	public String extractTaskType() throws InvalidInputException {
		return extractString("taskType");
	}
	
	@Override
	public String extractTaskName() throws InvalidInputException {
		return extractString("name");
	}
	
	@Override
	public String extractTaskDescription() throws InvalidInputException {
		return extractString("description");
	}
	
	@Override
	public Properties extractTaskInitializers() throws InvalidInputException {
		return extractProperties("init", false);
	}
	
	@Override
	public TaskList extractChildTasks() throws ServiceException, InvalidInputException {
		boolean parallelExecution = false;
		List<Task> tasks = extractChildTasks("tasks");
		if(tasks == null) {
			tasks = extractChildTasks("tasks.parallel");
			if(tasks == null)
				tasks = new ArrayList<Task>();
			parallelExecution = true;
		}
		return new TaskList(tasks, parallelExecution);
	}
	
	/**
	 * Returns the String from the properties and then removes
	 * it from the properties
	 * 
	 * @param name
	 * @return
	 * @throws InvalidInputException
	 */
	@Override
	public String extractString(String name) throws InvalidInputException {
		log.trace("Extracting string: {}", name);
		String value = getString(name);
		if(value != null)
			removeChild(name);
		return value;
	}

	/**
	 * Returns the String from the properties and then removes
	 * it from the properties
	 * 
	 * @param name
	 * @return
	 * @throws InvalidInputException
	 */
	@Override
	public Properties extractProperties(String name, boolean nullable) throws InvalidInputException {
		log.trace("Extracting properties: {}", name);
		Properties props = getProperties(name, nullable);
		removeChild(name);
		return props;
	}
	
	@Override
	public Properties getProperties(String name, boolean nullable) throws InvalidInputException {
		log.trace("Getting properties: {}", name);
		Properties props = new Properties();
		JsonTree propsTree = getChildTree(name);
		if(propsTree != null) {
			for(Object parameter : propsTree.keySet()) {
				log.trace("Adding property [{}]: {}", parameter.toString(), propsTree.getString(parameter.toString()));
				props.setProperty(parameter.toString(), propsTree.getString(parameter.toString()));
			}
		}
		else if (nullable) {
			return null;
		}
		
		return props;
	}

	@Override
	public Property extractProperty(String name, boolean nullable) throws InvalidInputException {
		Properties properties = extractProperties(name, nullable);
		if(properties == null)
			return null;
		
		if(properties.size() > 1)
			throw new InvalidInputException(name, "Multiple properties found: " + properties.toString());
		
		Object propertyName = properties.keys().nextElement();
		log.trace("Getting property [{}]: {}", propertyName, properties.getProperty(propertyName.toString()));
		return new Property(propertyName.toString(), properties.getProperty(propertyName.toString()));
	}

	private List<Task> extractChildTasks(String propertyName) throws ServiceException, InvalidInputException {
		
		List<?> childTasks = getChildList(propertyName);
		if(childTasks == null)
			return null;
		
		List<Task> tasks = new ArrayList<Task>();

		for(Object childTaskObject : childTasks) {
			if(childTaskObject instanceof LinkedTreeMap<?,?>) {
				ITaskProperties taskProperties = new JsonTaskProperties((LinkedTreeMap<?,?>) childTaskObject, taskBuilder);
				Task task = taskBuilder.buildTask(taskProperties);
				tasks.add(task);
			}
		}
		removeChild(propertyName);
		
		return tasks;
	}
}
