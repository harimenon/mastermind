package com.livincloud.mastermind.server.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.core.templates.TemplateManager;
import com.livincloud.mastermind.http.HttpServiceException;
import com.livincloud.mastermind.http.IRestService;
import com.livincloud.mastermind.server.ExtensionRegistry;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.ServerErrors;
import com.livincloud.mastermind.server.TaskRegistry;
import com.livincloud.mastermind.server.rest.resources.HostedTemplatesDTO;
import com.livincloud.mastermind.server.rest.resources.ScenarioDTO;
import com.livincloud.mastermind.server.rest.resources.TemplateDTO;
import com.livincloud.mastermind.util.LogUtils;

@Service
public class ScenarioBuilderService implements IScenarioBuilderService {

	@Autowired
	IRestService restService;
	
	@Autowired
	private TaskRegistry taskRegistry;
	
	@Autowired
    ExtensionRegistry extensionRegistry;
	
	/**
	 * Builds the scenario object from the DTO
	 * 
	 * @param scenarioDTO
	 * @return the Scenario object
	 * @throws ServiceException
	 */
	@Override
	public Scenario build(ScenarioDTO scenarioDTO) throws ServiceException {
		
		LogUtils.setContext(scenarioDTO.getName());
		
		// Load the templates specified in the DTO
		TemplateManager templateManager = new TemplateManager();
		loadTemplates(templateManager, scenarioDTO);
		
		// Build the Scenario object
		ScenarioBuilder scenarioBuilder = new ScenarioBuilder(new TaskBuilder(taskRegistry, extensionRegistry, templateManager, scenarioDTO.getExtensions()));
		return scenarioBuilder.build(scenarioDTO);
	}
	
	// Load the templates specified in the DTO
	private void loadTemplates(TemplateManager templateManager, ScenarioDTO scenarioDTO) throws ServiceException {
		
		// Load the hosted templates
		if(scenarioDTO.getHostedTemplates() != null) {
			for(String hostedTemplateName : scenarioDTO.getHostedTemplates().keySet()) {
				try {
					String templateUrl = scenarioDTO.getHostedTemplates().get(hostedTemplateName);
					ResponseEntity<HostedTemplatesDTO> response = restService.get(templateUrl, null, null, HostedTemplatesDTO.class);
					HostedTemplatesDTO templateListDTO = response.getBody();
					loadTemplates(templateManager, templateListDTO.getTemplates(), hostedTemplateName);
				} catch (HttpServiceException e) {
					throw new ServiceException(ServerErrors.INTERNAL_ERROR, "Error loading template [" + hostedTemplateName + "] - " + e.getMessage());
				}
			}
		}

		// Load the templates defined in the DTO (may override the ones in the hosted templates)
		if(scenarioDTO.getTemplates() != null ) {
			loadTemplates(templateManager, scenarioDTO.getTemplates(), "templates");
		}
	}
	
	private void loadTemplates(TemplateManager templateManager, List<TemplateDTO> dtos, String templatePrefix) throws ServiceException {
		for(TemplateDTO templateDTO : dtos) {
			templateManager.addTemplate(templatePrefix, templateDTO.toTemplate());
		}
	}
}
