package com.livincloud.mastermind.server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.core.services.IScenarioExecutionService;
import com.livincloud.mastermind.http.IRestService;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.ServerErrors;
import com.livincloud.mastermind.server.domain.ExecutionRecord;
import com.livincloud.mastermind.server.rest.resources.ExecutionRequestDTO;
import com.livincloud.mastermind.server.rest.resources.ScenarioDTO;
import com.livincloud.mastermind.util.IdGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExecutionRequestProcessor implements IExecutionRequestProcessor {
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private IScenarioExecutionService iScenarioExecutionService;

	@Autowired
	private IRestService restService;
	
	@Autowired
	private IScenarioBuilderService scenarioBuilderService;
	
	@Override
	public ExecutionRecord run(ExecutionRequestDTO executionRequestDTO) throws ServiceException {
		
		try {
			// Unique ID for the request
			String executionId = IdGenerator.generateGuid();
			
			// Build the Scenario
			ScenarioDTO scenarioDTO = ScenarioDTO.fromScenarioMap(executionRequestDTO.getScenario());
	  		Scenario scenario = scenarioBuilderService.build(scenarioDTO);
	  		
			ExecutionRecord executionRecord = createExecutionRecord(executionId, executionRequestDTO, scenarioDTO);
			
			log.info("Execution STARTED: {} ({}) [{}/{}]", scenario.getName(), executionId, executionRequestDTO.getThreadCount(), executionRequestDTO.getThreadCount());
			
			ScenarioExecutionTracker scenarioExecutionTracker = new ScenarioExecutionTracker(executionId);
			
			// Start the threads 
			for(int i = 0; i < executionRequestDTO.getThreadCount(); i++) {
				new Thread( new Runnable() {
					public void run() {
						// Execute the scenario
						iScenarioExecutionService.execute(
									scenario,
									new RestScenarioStatusListener(restService, executionId, executionRequestDTO, scenarioExecutionTracker));
					}
				}).start();
			}
			
			return executionRecord;
		}
		catch (InvalidInputException e) {
			log.error("Invalid input: ", e.getMessage());
			throw new ServiceException(ServerErrors.INVALID_INPUT, e.getMessage(), e.getInputString());
		}
	}
	
	private ExecutionRecord createExecutionRecord(String executionId, ExecutionRequestDTO executionRequestDTO, ScenarioDTO scenarioDTO) throws ServiceException {
		ExecutionRecord executionRecord;
		try {
			executionRecord = new ExecutionRecord(
					executionId, 
					executionRequestDTO.getFlowId(),
					scenarioDTO.getName(),
					mapper.writeValueAsString(scenarioDTO),
					executionRequestDTO.getThreadCount(),
					executionRequestDTO.getExecutionStatusUrl());
		} catch (JsonProcessingException e) {
			 throw new ServiceException(ServerErrors.INTERNAL_ERROR, "Error saving execution record: " + e.getMessage());
		}
		
		return executionRecord;
	}
	
	class ScenarioExecutionTracker {
		int scenarioExecutionCount = 0;
		String executionId;
		
		ScenarioExecutionTracker(String executionId) {
			this.executionId = executionId;
		}
		
		synchronized void notifyScenarioStart() {
			scenarioExecutionCount++;
		}
		
		synchronized void notifyScenarioEnd() {
			if((--scenarioExecutionCount) == 0) {
				log.info("Execution ENDED: {}", executionId);
			}
		}
	}
}
