package com.livincloud.mastermind.server.services;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.Scenario;
import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.rest.resources.ScenarioDTO;

public interface IScenarioBuilderService {
	Scenario build(ScenarioDTO scenarioDTO) throws ServiceException, InvalidInputException;
}
