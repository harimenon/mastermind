package com.livincloud.mastermind.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.livincloud.mastermind.InvalidInputException;
import com.livincloud.mastermind.core.domain.ITaskFactory;
import com.livincloud.mastermind.core.domain.ITaskProperties;
import com.livincloud.mastermind.core.domain.Task;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ConfigurationProperties(prefix = "task-definitions")
public class TaskRegistry {
    
	@Getter @Setter private List<TaskDefinition> tasks;
	private Map<String, ITaskFactory> taskFactories = new HashMap<String, ITaskFactory>();
	
	public void registerTasks(Application.ClassLoader classLoader) throws ServiceException {
		if(tasks == null) {
			throw new ServiceException(ServerErrors.INVALID_CONFIGURATION, "No task registered on server");
		}
		
		log.trace("Registering tasks: {}", tasks.size());
		for(TaskDefinition task : tasks) {
			log.trace("Loading extension: {} - {} - {}", task.getName(), task.getFactory(), task.getPrimaryProperties().toString());
			taskFactories.put(task.getName(), classLoader.loadClass(task.getFactory(), ITaskFactory.class));
		}
		log.info("Tasks registered: {}", taskFactories.keySet().toString());
	}
	
	public Task createTask(ITaskProperties taskProperties) throws InvalidInputException, ServiceException {
		if(tasks == null) {
			throw new ServiceException(ServerErrors.INVALID_CONFIGURATION, "No task registered on server");
		}
		
		String taskType = taskProperties.extractTaskType();
		if(taskType == null) {
			taskType = deduceTaskType(taskProperties);
			if(taskType == null) {
				throw new InvalidInputException(taskProperties.toString(), "Unknown task!");
			}
		}
		
		return createTask(taskType, taskProperties);
	}
	
	private String deduceTaskType(ITaskProperties taskProperties) {
		log.trace("Task type not defined - deducing based on properties...");
		for(TaskDefinition taskDefinition : tasks) {
			log.trace("Primary properties [{}]: {}", taskDefinition.getName(), taskDefinition.getPrimaryProperties());
			for(Object prop : taskProperties.getPropertyNames()) {
				log.trace("Checking property: {}", prop);
				if(taskDefinition.primaryProperties.stream().anyMatch(prop.toString()::equalsIgnoreCase))
					return taskDefinition.getName();
			}
		}
		
		return null;
	}

	private Task createTask(String taskType, ITaskProperties taskProperties) throws InvalidInputException, ServiceException {
		ITaskFactory factory = taskFactories.get(taskType);
		if(factory == null)
			throw new InvalidInputException(taskType, "Unknown task type: " + taskType);
		return factory.createTask(taskProperties);
	}
	
	@Data
	static class TaskDefinition {
		String name;
		String factory;
		List<String> primaryProperties;
	}
}
