package com.livincloud.mastermind.server.rest.resources;

import com.livincloud.mastermind.server.domain.ScenarioExecutionStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScenarioExecutionStatusDTO {
	private String executionContextId;
	private ScenarioExecutionStatus.State status;
	
	public static ScenarioExecutionStatusDTO fromExecutionStatus(ScenarioExecutionStatus scenarioExecutionStatus)  {
		return new ScenarioExecutionStatusDTO(scenarioExecutionStatus.getExecutionContextId(), scenarioExecutionStatus.getStatus());
	}
	
	public ScenarioExecutionStatus toExecutionStatus()  {
		return new ScenarioExecutionStatus(executionContextId, status);
	}
}
