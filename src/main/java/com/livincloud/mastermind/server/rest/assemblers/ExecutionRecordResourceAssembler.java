package com.livincloud.mastermind.server.rest.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.domain.ExecutionRecord;
import com.livincloud.mastermind.server.rest.controllers.ExecutionRequestController;
import com.livincloud.mastermind.server.rest.resources.ExecutionRecordDTO;
import com.livincloud.mastermind.server.rest.resources.ExecutionRecordResource;

@Component
public class ExecutionRecordResourceAssembler implements ResourceAssembler<ExecutionRecord, ExecutionRecordResource> {

	@Override
	public ExecutionRecordResource toResource(ExecutionRecord executionRecord) {
		ExecutionRecordResource resource = new ExecutionRecordResource(ExecutionRecordDTO.fromExecutionRecord(executionRecord));
		try {
			resource.add(linkTo(methodOn(ExecutionRequestController.class).getExecutionRequestById(executionRecord.getExecutionId())).withSelfRel());
			
			//Link link = new Link(dto.getStatusCallbackUrl().replace("host.docker.internal","localhost") + "/" +
			//		methodOn(ExecutionStatusController.class).getExecutionStatus(dto.getId()));

			if(executionRecord.getStatusCallbackUrl() != null)
				resource.add(new Link(executionRecord.getStatusCallbackUrl().replace("host.docker.internal","localhost") + "/results/" + executionRecord.getExecutionId()));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return resource;
	}
}
