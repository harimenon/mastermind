package com.livincloud.mastermind.server.rest.resources;

import com.livincloud.mastermind.server.domain.ExecutionRecord;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExecutionRecordDTO {
	private String executionRequestId;
	
	private String flowId;

	private String scenarioName;
	
	private String scenarioJson;
	
	private int threadCount;
	
	private String statusCallbackUrl;
	
	public static ExecutionRecordDTO fromExecutionRecord(ExecutionRecord executionRecord) {
		return new ExecutionRecordDTO(
				executionRecord.getExecutionId(), 
				executionRecord.getFlowId(), 
				executionRecord.getScenarioName(),
				executionRecord.getScenarioJson(),
				executionRecord.getThreadCount(), 
				executionRecord.getStatusCallbackUrl());
	}
}
