package com.livincloud.mastermind.server.rest.resources;

import java.util.Properties;

import lombok.Data;

@Data
public class ExtensionDTO {
	String extensionName;
	Properties extensionProperties;
}
