package com.livincloud.mastermind.server.rest.resources;

import java.util.List;
import java.util.Properties;

import lombok.Data;

@Data
public class RestCallDTO {
	private String name;
	private String http_get;
	//private String POST;
	private String body = "";
	private String forkOn;
	private Properties inits;
	private List<String> asserts;
	private List<String> extensions;
}
