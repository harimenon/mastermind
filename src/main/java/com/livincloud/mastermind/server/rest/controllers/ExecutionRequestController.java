package com.livincloud.mastermind.server.rest.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.domain.ExecutionRecord;
import com.livincloud.mastermind.server.persistence.service.IExecutionRecordStore;
import com.livincloud.mastermind.server.rest.assemblers.ExecutionRecordResourceAssembler;
import com.livincloud.mastermind.server.rest.resources.ExecutionRecordResource;
import com.livincloud.mastermind.server.rest.resources.ExecutionRequestDTO;
import com.livincloud.mastermind.server.services.IExecutionRequestProcessor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController("ExecutionRequestControllerV1")
@EnableHypermediaSupport(type = HypermediaType.HAL)
@RequestMapping(value = "/api/execution", produces = { "application/hal+json" })
public class ExecutionRequestController {
	
	@Autowired
	IExecutionRequestProcessor iExecutionRequestProcessor;

	@Autowired
	private ExecutionRecordResourceAssembler executionRecordResourceAssembler;
	
	@Autowired
	private IExecutionRecordStore iExecutionRecordStore;

	
	@RequestMapping(method = RequestMethod.POST, value = "/requests", produces = { "application/hal+json" })
	public HttpEntity<ExecutionRecordResource> addExecutionRequest(
			@RequestBody ExecutionRequestDTO executionRequestDTO)
			throws ServiceException {
		
		try {
			ExecutionRecord executionRecord = iExecutionRequestProcessor.run(executionRequestDTO);
			
			iExecutionRecordStore.saveExecutionRecord(executionRecord);
			
			ExecutionRecordResource resource = executionRecordResourceAssembler.toResource(executionRecord);
			return new ResponseEntity<>(resource, HttpStatus.OK);
		}
		catch(ServiceException e) {
			log.error("Execution failed [{}]: {}", executionRequestDTO.getScenarioName(), e.getMessage());
			throw e;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/requests/{id}", produces = { "application/hal+json" })
	public HttpEntity<ExecutionRecordResource> getExecutionRequestById(
			@PathVariable("id") String executionRequestId)
			throws ServiceException {
		
		ExecutionRecord executionRecord = iExecutionRecordStore.getExecutionRecord(executionRequestId);
		if(executionRecord == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		
		ExecutionRecordResource resource = executionRecordResourceAssembler.toResource(executionRecord);
		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/requests", produces = { "application/hal+json" })
	public HttpEntity<List<ExecutionRecordResource>> getExecutionRequests()
			throws ServiceException {
		
		List<ExecutionRecordResource> requestList = new ArrayList<ExecutionRecordResource>();
		iExecutionRecordStore.getAllExecutionRecords().forEach(
				executionRecord -> requestList.add(executionRecordResourceAssembler.toResource(executionRecord)));
		
		return new ResponseEntity<>(requestList, HttpStatus.OK);
	}
}
