package com.livincloud.mastermind.server.rest.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.domain.ExecutionSummary;
import com.livincloud.mastermind.server.domain.TaskExecutionStatus;
import com.livincloud.mastermind.server.persistence.service.IExecutionStatusStore;
import com.livincloud.mastermind.server.rest.assemblers.ExecutionSummaryResourceAssembler;
import com.livincloud.mastermind.server.rest.resources.ExecutionSummaryResource;
import com.livincloud.mastermind.server.rest.resources.ScenarioExecutionStatusDTO;
import com.livincloud.mastermind.server.rest.resources.TaskExecutionStatusDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController("ExecutionResponseControllerV1")
@RequestMapping(value = "/api/execution", produces = { "application/hal+json" })
public class ExecutionStatusController {
	
	@Autowired
	private IExecutionStatusStore iExecutionStatusStore;
	
	@Autowired
	private ExecutionSummaryResourceAssembler executionSummaryResourceAssembler;
	
	@RequestMapping(method = RequestMethod.POST, value = "/results/{executionRequestId}/scenarios", produces = { "application/hal+json" })
	public HttpEntity<ExecutionSummaryResource> addScenarioExecutionStatus(
			@PathVariable("executionRequestId") String executionRequestId,
			@RequestBody ScenarioExecutionStatusDTO scenarioExecutionStatusDTO)
			throws ServiceException {
		
		String id = executionRequestId + "." + scenarioExecutionStatusDTO.getExecutionContextId();
		log.debug("Saving execution status [{}]: {}", id, scenarioExecutionStatusDTO.toString());
		ExecutionSummary executionSummary = iExecutionStatusStore.saveExecutionStatus(executionRequestId, scenarioExecutionStatusDTO.toExecutionStatus());
		
		return new ResponseEntity<>(executionSummaryResourceAssembler.toResource(executionSummary), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/results/{executionRequestId}", produces = { "application/hal+json" })
	public HttpEntity<ExecutionSummaryResource> getExecutionSummary(
			@PathVariable("executionRequestId") String executionRequestId)
			throws ServiceException {
		
		ExecutionSummary executionSummary = iExecutionStatusStore.getExecutionStatus(executionRequestId);
		if(executionSummary == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<>(executionSummaryResourceAssembler.toResource(executionSummary), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/results", produces = { "application/hal+json" })
	public HttpEntity<Collection<ExecutionSummaryResource>> getAllExecutionSummaries()
			throws ServiceException {
		
		List<ExecutionSummaryResource> executionStatusList = new ArrayList<ExecutionSummaryResource>();
		iExecutionStatusStore.getExecutionStatusList().forEach(executionSummary -> executionStatusList.add(executionSummaryResourceAssembler.toResource(executionSummary)));
		return new ResponseEntity<>(executionStatusList, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/results/purge", produces = { "application/hal+json" })
	public void purgeStatuses()
			throws ServiceException {
		
		/*
		LOGGER.info("Purging all stats: " + stats.toString());
		
		synchronized(lock) {
			stats = new HashMap<String, List<Long>>();
		}
		*/
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/results/{executionRequestId}/tasks", produces = { "application/hal+json" })
	public HttpEntity<TaskExecutionStatusDTO> addTaskStatus(
			@PathVariable("executionRequestId") String executionRequestId,
			@RequestBody TaskExecutionStatusDTO taskExecutionStatusDTO)
			throws ServiceException {
		
		String id = executionRequestId + "." + taskExecutionStatusDTO.getExecutionContextId() + "." + taskExecutionStatusDTO.getTaskName();
		log.debug("Saving task status [{}]: {}", id, taskExecutionStatusDTO.toString());
		
		iExecutionStatusStore.saveTaskStatus(executionRequestId, taskExecutionStatusDTO.toTaskStatus());
		return new ResponseEntity<>(taskExecutionStatusDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/results/{executionRequestId}/tasks/{taskName}", produces = { "application/hal+json" })
	public HttpEntity<TaskExecutionStatusDTO> getTaskStatus(
			@PathVariable("executionRequestId") String executionRequestId,
			@PathVariable("taskName") String taskName)
			throws ServiceException {
		
		TaskExecutionStatus taskExecutionStatus = iExecutionStatusStore.getTaskStatus(executionRequestId, taskName);
		if(taskExecutionStatus == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<>(TaskExecutionStatusDTO.fromTaskStatus(taskExecutionStatus), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/results/{executionRequestId}/tasks", produces = { "application/hal+json" })
	public HttpEntity<Map<String, Map<String, TaskExecutionStatusDTO>>> getAllTaskStatuses(
			@PathVariable("executionRequestId") String executionRequestId)
			throws ServiceException {
		
		Map<String, Map<String, TaskExecutionStatus>> storedStatuses = iExecutionStatusStore.getTaskStatusList(executionRequestId);
		if(storedStatuses == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		
		Map<String, Map<String, TaskExecutionStatusDTO>> results = new HashMap<String, Map<String, TaskExecutionStatusDTO>>();
		
		storedStatuses.forEach((contextId, storedTaskStatusMap) -> {
			
			Map<String, TaskExecutionStatusDTO> taskStatusMap = new HashMap<String, TaskExecutionStatusDTO>();
			results.put(contextId, taskStatusMap);
			
			storedTaskStatusMap.forEach((taskName,taskStatus) -> taskStatusMap.put(taskName, TaskExecutionStatusDTO.fromTaskStatus(taskStatus)));
		});
		
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
}
