package com.livincloud.mastermind.server.rest.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController("TemplatesControllerV1")
@RequestMapping(value = "/api", produces = { "application/hal+json" })
public class TemplatesController {

	@RequestMapping(method = RequestMethod.GET, value = "/templates/{templateId}", produces = { "application/hal+json" })
	public HttpEntity<String> getTemplate(@PathVariable String templateId) {
		try {
			Resource resource = new ClassPathResource("templates/" + templateId + ".json");
			InputStream in = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	        StringBuilder out = new StringBuilder();
	        String line;
	        while ((line = reader.readLine()) != null) {
	            out.append(line);
	        }
	        reader.close();
	        return new ResponseEntity<>(out.toString(), HttpStatus.OK);
	    } catch (IOException e) {
		}
		
        return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
	}
}
