package com.livincloud.mastermind.server.rest.resources;

import java.util.HashMap;
import java.util.Map;

import com.livincloud.mastermind.server.domain.TaskExecutionStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskExecutionStatusDTO {
	private String executionRequestId;
	private String executionContextId;
	private String taskName;
	private TaskExecutionStatus.State executionState = TaskExecutionStatus.State.EXECUTING;
	private Map<String,Object> results = new HashMap<String,Object>();
	
	private TaskExecutionStatusDTO(String executionRequestId, String executionContextId, String taskName) {
		this.executionRequestId = executionRequestId;
		this.executionContextId = executionContextId;
		this.taskName = taskName;
	}
	
	public static TaskExecutionStatusDTO fromTaskStatus(TaskExecutionStatus taskExecutionStatus) {
		return new TaskExecutionStatusDTO(
				taskExecutionStatus.getExecutionRequestId(),
				taskExecutionStatus.getExecutionContextId(),
				taskExecutionStatus.getTaskName(),
				taskExecutionStatus.getTaskStatus(),
				taskExecutionStatus.getResults());
	}
	
	public TaskExecutionStatus toTaskStatus() {
		return new TaskExecutionStatus(
				getExecutionRequestId(),
				getExecutionContextId(),
				getTaskName(),
				getExecutionState(),
				getResults());
	}
	
	public static class Builder {
		
		TaskExecutionStatusDTO dto;
		
		public Builder(String executionRequestId, String executionContextId, String taskName) {
			dto = new TaskExecutionStatusDTO(executionRequestId, executionContextId, taskName);
		}

		public Builder setState(TaskExecutionStatus.State state) {
			dto.executionState = state; 
			return this;
		}
		
		public Builder addResult(String name, Object value) {
			dto.results.put(name, value);
			return this;
		}
		
		public Builder addResults(Map<String,Object> results) {
			dto.results.putAll(results);
			return this;
		}
		
		public TaskExecutionStatusDTO build() {
			return dto;
		}
	}
}
