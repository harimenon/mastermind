package com.livincloud.mastermind.server.rest.resources;

import java.util.ArrayList;
import java.util.List;

import com.livincloud.mastermind.server.domain.ExecutionSummary;

import lombok.Data;

@Data
public class ExecutionSummaryDTO {
	private String executionRequestId;
	private List<ScenarioExecutionStatusDTO> scenarioStatusList = new ArrayList<ScenarioExecutionStatusDTO>();
	
	public ExecutionSummaryDTO(String executionRequestId) {
		this.executionRequestId = executionRequestId;
	}
	
	public void addScenarioStatus(ScenarioExecutionStatusDTO statusDTO) {
		scenarioStatusList.add(statusDTO);
	}
	
	public static ExecutionSummaryDTO fromExecutionSummary(ExecutionSummary resultSummary) {
		ExecutionSummaryDTO resultSummaryDTO = new ExecutionSummaryDTO(resultSummary.getExecutionRequestId());
		resultSummary.getScenarioStatusList().forEach(scenarioExecutionStatus -> resultSummaryDTO.addScenarioStatus(ScenarioExecutionStatusDTO.fromExecutionStatus(scenarioExecutionStatus)));
		
		return resultSummaryDTO;
	}
}
