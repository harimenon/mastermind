package com.livincloud.mastermind.server.rest.resources;

import java.util.List;

import lombok.Data;

@Data
public class HostedTemplatesDTO {
	private List<TemplateDTO> templates;
}
