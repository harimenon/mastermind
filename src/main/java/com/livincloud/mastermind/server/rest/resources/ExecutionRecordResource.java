package com.livincloud.mastermind.server.rest.resources;

import org.springframework.hateoas.Resource;

public class ExecutionRecordResource extends Resource<ExecutionRecordDTO> {

	public ExecutionRecordResource(ExecutionRecordDTO dto) {
		super(dto);
	}
}
