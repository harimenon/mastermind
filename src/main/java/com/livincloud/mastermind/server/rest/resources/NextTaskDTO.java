package com.livincloud.mastermind.server.rest.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NextTaskDTO {
	String taskName;
	
	@JsonProperty(required = false)
	String condition;
}
