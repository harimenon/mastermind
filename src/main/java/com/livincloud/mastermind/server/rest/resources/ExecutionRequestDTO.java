package com.livincloud.mastermind.server.rest.resources;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExecutionRequestDTO {
	
	private String flowId;

	private Map<String, Object> scenario;
	
	private int threadCount = 1;
	
	private String executionStatusUrl;
	
	public String getScenarioName() {
		return (scenario != null) && (scenario.get("name") != null) ? scenario.get("name").toString() : "<unknown>";
	}
}
