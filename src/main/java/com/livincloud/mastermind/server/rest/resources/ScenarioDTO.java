package com.livincloud.mastermind.server.rest.resources;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.InvalidInputException;

import lombok.Data;

@Data
public class ScenarioDTO {

	private String name;
	
	private String description;
	
	private Map<String, Properties> extensions;
	
	private List<LinkedTreeMap<?,?>> tasks;
	
	private boolean executeTasksInParallel = false;
	
	private Map<String, Object> input;

	private List<TemplateDTO> templates;

	private Map<String, String> hostedTemplates;
	
	public static ScenarioDTO fromScenarioMap(Map<String, Object> scenarioMap) throws InvalidInputException {
		try {
			if(scenarioMap.containsKey("tasks.parallel")) {
				scenarioMap.put("tasks", scenarioMap.get("tasks.parallel"));
				scenarioMap.put("executeTasksInParallel", true);
				scenarioMap.remove("tasks.parallel");
			}
			return new ObjectMapper().convertValue(scenarioMap, ScenarioDTO.class);
		}
		catch(Exception e) {
			throw new InvalidInputException(scenarioMap.toString(), e.getMessage());
		}
	}
}
