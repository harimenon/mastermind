package com.livincloud.mastermind.server.rest.resources;

import com.google.gson.internal.LinkedTreeMap;
import com.livincloud.mastermind.core.templates.Template;

import lombok.Data;

@Data
public class TemplateDTO {
	
	private String type;
	private String name;
	private LinkedTreeMap<?,?> value;
	
	public Template toTemplate() {
		return new Template(type, name, value);
	}
}
