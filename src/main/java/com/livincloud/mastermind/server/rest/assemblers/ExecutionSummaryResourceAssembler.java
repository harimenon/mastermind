package com.livincloud.mastermind.server.rest.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import com.livincloud.mastermind.server.ServiceException;
import com.livincloud.mastermind.server.domain.ExecutionSummary;
import com.livincloud.mastermind.server.rest.controllers.ExecutionRequestController;
import com.livincloud.mastermind.server.rest.controllers.ExecutionStatusController;
import com.livincloud.mastermind.server.rest.resources.ExecutionSummaryDTO;
import com.livincloud.mastermind.server.rest.resources.ExecutionSummaryResource;

@Component
public class ExecutionSummaryResourceAssembler implements ResourceAssembler<ExecutionSummary, ExecutionSummaryResource> {

	@Override
	public ExecutionSummaryResource toResource(ExecutionSummary executionSummary) {
		ExecutionSummaryResource resource = new ExecutionSummaryResource(ExecutionSummaryDTO.fromExecutionSummary(executionSummary));
		try {
			resource.add(linkTo(methodOn(ExecutionRequestController.class).getExecutionRequestById(executionSummary.getExecutionRequestId())).withSelfRel());
			
			if(executionSummary.getScenarioStatusList() != null) {
				resource.add(linkTo(
						methodOn(ExecutionStatusController.class).getAllTaskStatuses(executionSummary.getExecutionRequestId())).withSelfRel());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return resource;
	}
}
