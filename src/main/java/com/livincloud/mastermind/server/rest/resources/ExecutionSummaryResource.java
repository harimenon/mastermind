package com.livincloud.mastermind.server.rest.resources;

import org.springframework.hateoas.Resource;

public class ExecutionSummaryResource extends Resource<ExecutionSummaryDTO> {

	public ExecutionSummaryResource(ExecutionSummaryDTO dto) {
		super(dto);
	}
}
