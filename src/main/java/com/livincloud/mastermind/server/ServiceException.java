package com.livincloud.mastermind.server;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.livincloud.mastermind.IErrorCode;
import com.livincloud.mastermind.MasterMindException;


/**
 * ApiException
 * 
 * @author hari
 *
 */
public class ServiceException extends MasterMindException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5342195811874784496L;

	public ServiceException(IErrorCode error) {
		super(getErrorString(error.getCode(), error.getDescription()));
	}

	public ServiceException(ServerErrors error, Object... params) {
		super(getErrorString(error.getCode(), error.addAttributes(params).getDescription()));
	}

	private static String getErrorString(int errorCode, String errorDescription) {

		Map<String, Object> errorObject = new HashMap<String, Object>();

		errorObject.put("Code", errorCode);
		errorObject.put("Description", errorDescription);

		String errorString="InternalError";
		try {
			errorString = new ObjectMapper().writeValueAsString(errorObject);
		} catch (JsonProcessingException e) {
			System.out.println("----------------------");
			e.printStackTrace();
		}
		return errorString;
	}
}
