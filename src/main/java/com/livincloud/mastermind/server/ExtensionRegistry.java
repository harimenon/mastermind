package com.livincloud.mastermind.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.livincloud.mastermind.extensions.IExtensionFactory;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ConfigurationProperties(prefix = "task-extensions")
public class ExtensionRegistry {
    
	@Getter @Setter private List<ExtensionDefinition> extensions;
	private Map<String, IExtensionFactory> extensionFactories = new HashMap<String, IExtensionFactory>();
	
	public void registerExtensions(Application.ClassLoader classLoader) throws ServiceException {
		if(extensions != null) {
			log.trace("Registering extensions: {}", extensions.size());
			for(ExtensionDefinition extension : extensions) {
				log.trace("Loading extension: {} - {}", extension.getName(), extension.getFactory());
				extensionFactories.put(extension.getName(), classLoader.loadClass(extension.getFactory(), IExtensionFactory.class));
			}
			log.info("Extensions registered: {}", extensionFactories.keySet().toString());
		}
	}
	
	public IExtensionFactory getExtensionFactory(String type) {
		return extensionFactories.get(type);
	}
	
	@Data
	static class ExtensionDefinition {
		String name;
		String factory;
	}
}
